import { useFormik } from "formik";
import Cookies from "js-cookie";
import Image from "next/image";
import { useRouter } from "next/router";
import { useState } from "react";
import { useQuery } from "react-query";
import { useSelector } from "react-redux";
import Swal from "sweetalert2";
import { BreadCrumb, Button, ChooseFoto, TextLabel, ProtectedRoute } from "../../../components";
import Layout from "../../../layout";
import { addRestaurant, detailRestaurant } from "../../../services/restaurantService";
import { BASE_URL } from "../../../utils/constants";
import { restaurantAddValidation } from "../../../utils/validations/restaurantAddValidation";
import LoadingProduct from "../../products/LoadingProduct";

function detail() {
    const router = useRouter()
    const {id} = router.query;
    const token = Cookies.get('token');
    const [Restaurant, setRestaurant] = useState({
        restaurant_image:false,
        name:'',
        location:'',
        status:false,
    })

    const config = {
        headers: { 'Authorization': token },
        params:{id:id}
    }
    
    const { isLoading, isError, error } = useQuery(
        ['restaurants'],
        () => detailRestaurant(config),
        {
            onSuccess: (restaurants) => {
                setRestaurant(restaurants)
            },
        }
    )

    console.log('setRestaurant :', BASE_URL+Restaurant?.data?.restaurant_image)

    return (
        <Layout>
            {
                isLoading ?
                <LoadingProduct/>
                :
                <div className='md:mx-11'>
                    <BreadCrumb 
                        directions={[
                        { title:'Detail Restaurant', link:'#'},
                        ]}
                    />
                    <div>
                        {/* label, name, placeholder, type, value, required */}
                        <div className="w-full mt-3 mb-6">
                            <div className="w-full max-h-24 justify-center items-center">
                                {
                                    Restaurant?.status && Restaurant?.data?.restaurant_image && (
                                        <Image width={100} height={100} alt="product image" className='rounded-lg' src={`${BASE_URL}${Restaurant?.data?.restaurant_image}`}/>
                                    )
                                }
                            </div>
                        </div>
                        <TextLabel
                            name="name"
                            label="Restaurant Name"
                            placeholder="Restaurant Name"
                            type="text"
                            value={Restaurant?.data?.name}
                        />
                        <TextLabel
                            name="location"
                            label="Location"
                            placeholder="Location"
                            type="text"
                            value={Restaurant?.data?.location}
                        />
                        <div className="flex items-start mb-6">
                            <div className="flex items-center h-5">
                                <input name="status" type="checkbox" value={Restaurant?.data?.status} 
                                    className="w-4.5 h-4.5 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300"
                                    checked={Restaurant?.data?.status}
                                />
                            </div>
                            <label htmlFor="remember" className="ml-2 text-sm font-medium text-gray-900">Status</label>
                        </div>
                        {/* label, type, bgColor, bgHover, color, colorHover, onClick */}
                    </div>
                </div>
            }
        </Layout>
    )
}

export default ProtectedRoute(detail)
