import Cookies from "js-cookie";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { MdAdd } from 'react-icons/md';
import { useMutation, useQuery, useQueryClient } from "react-query";
import { ActionButton, BreadCrumb, Button, LoadingTable, ProtectedRoute } from "../../components";
import PaginationComponentTransaction from "../../components/Pagination";
import Layout from "../../layout";
import { deleteRestaurant, getRestaurant } from "../../services/restaurantService";
import { BASE_URL } from "../../utils/constants";

function Index() {
  const router = useRouter()
  // const [restaurants, setRestaurants] = useState([])
  const [totalPage, setTotalPage] = useState(1)
  const [page, setPage] = useState(1)
  const [search, setSearch] = useState(null)
  const token = Cookies.get('token');
  const config = {headers:{Authorization:token}, params:{limit:3, offset:1}}
  const queryClient = useQueryClient()

  const { isLoading, data:restaurants, refetch, isError, error } = useQuery(
    ['restaurants', page, search],
    () => getRestaurant({headers:{Authorization:token}, params:{limit:3, offset:page, name:search}})
    // ,
    // {
    //   onSuccess: (restaurants) => {
    //     setRestaurants(restaurants)
    //   },
    // }
  )

  const {mutate} = useMutation(`restaurants`, deleteRestaurant, {
    onSuccess: () => {
      queryClient.invalidateQueries('restaurants')
    },
  })

  const handleMutate = async (config) => {
    mutate(config)
  }

  const handleClick = async (action, id) => {
    switch (action) {
      case 'detail':
        router.push(`/restaurants/detail/${id}`)
        break;
      case 'edit':
        router.push(`/restaurants/edit/${id}`)
        break;
      case 'delete':
        await handleMutate({headers:{Authorization:token}, params:{id:id}})
        break;
      default:
        break;
    }
  }

  return (
    <>
      {
        isLoading ? (
          <Layout>
            <div className="w-full flex justify-between h-9">
                <BreadCrumb 
                  directions={[
                    { title:'Resaurants', link:'#'},
                  ]}
                />
      
                <Link href="/restaurants/add">
                  <Button
                      label="Add"
                      type="button"
                      bgColor="#1ABC9C"
                      bgHover="#1ABC9C"
                      color="white"
                      icon={<MdAdd className="h-5 w-5 font-semibold text-white"/>}
                  />
                </Link>
              </div>
            <LoadingTable/>
          </Layout>
        ): (
          <Layout>
            <div className='md:mx-11'>
              <div className="w-full flex justify-between h-9">
                <BreadCrumb 
                  directions={[
                    { title:'Restaurants', link:'#'},
                  ]}
                />
      
                <Link href="/restaurants/add">
                  <Button
                      label="Add"
                      type="button"
                      bgColor="#1ABC9C"
                      bgHover="#1ABC9C"
                      color="white"
                      icon={<MdAdd className="h-5 w-5 font-semibold text-white"/>}
                  />
                </Link>
              </div>
              <div className="flex flex-col">
                <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
                  <div className="py-4 inline-block min-w-full sm:px-6 lg:px-8">
                    <div className="overflow-hidden">
                      <table className="min-w-full text-center">
                        <thead className="border-b bg-[#FFC700]">
                          <tr>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              No
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3 text-left">
                              Restaurant Image
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Restaurant Name
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Status
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Action
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            restaurants.status && restaurants.data.length > 0 ? (
                              restaurants.data.map((restaurants, index) => (
                                <tr className="bg-white border-b hover:bg-gray-200">
                                  <td className="px-4 py-3 text-sm text-gray-900">{index+1}</td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    <Image width={60} height={60} alt="product image" src={`${BASE_URL}${restaurants?.restaurant_image}`}/>
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    {restaurants?.name}
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    {restaurants?.status? 'Active' : 'Inactive'}
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    <ActionButton id={restaurants.id} onClick={(action, id) => handleClick(action, id)}/>
                                  </td>
                                </tr>
                              ))
                            ) :
                            <tr className="bg-white border-b hover:bg-gray-200">
                              <td className="px-4 py-3 text-sm text-gray-900" colSpan={6}>Data empty</td>
                            </tr>
                          }
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              <div className="w-full flex justify-center">
                <PaginationComponentTransaction
                  page={restaurants?.page ?? 0}
                  between={3}
                  total={restaurants?.total ?? 0}
                  limit={3}
                  changePage={(page) => {
                    setPage(page)
                    refetch()
                  }}
                  ellipsis={1}
                />
              </div>
          </Layout>
        )
      }
    </>
  )
}

export default ProtectedRoute(Index)

// export async function getStaticProps() {
//   const restaurants = await getProduct({params:{limit:3, offset:1}})
//   return {
//     props: {
//       restaurants,
//     },
//   }
// }
