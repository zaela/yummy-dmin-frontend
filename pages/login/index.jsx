import { useFormik } from 'formik'
import Image from 'next/image'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { Button, InputLabel } from '../../components'
import { setLogin } from '../../services/authService'
import { loginValidation } from '../../utils/validations/authValidation'
import LoginImage from './../../public/assets/login-image.svg'
import { loginSuccess } from '../../redux/slice/auth'
import Cookies from 'js-cookie'

function index() {
    const dispatch = useDispatch();
    const router = useRouter()

    const onSubmit = async () => {
        const user = await setLogin({data:values})
        const { id, role, accessToken, refreshToken } = user.data;
        console.log('user.data :', user.data)

        Cookies.set('id', id, { expires: 1 });//expired 1 day
        Cookies.set('role', role, { expires: 1 });//expired 1 day
        Cookies.set('token', accessToken, { expires: 1 });//expired 1 day
        Cookies.set('refresh_token', refreshToken, { expires: 1 });//expired 1 day
        dispatch(loginSuccess(user.data))
        if (user.status) {
            if (window.history.length > 1 && document.referrer.indexOf(window.location.host) !== -1) {
                router.back();
            } else {
                router.push('/products')
                // router.replace(PATH.home);
            }
        }
    }

    const { values, errors, touched, isSubmitting, handleBlur, handleChange, handleSubmit} = useFormik({
        initialValues:{
            email:'',
            password:''
        },
        validationSchema:loginValidation,
        onSubmit,
    })

    return (
        <div className='bg-white min-h-screen w-full flex items-center'>
            <div className='w-1/2 flex items-center justify-center'>
                <div className='w-1/2'>
                    <Image className='w-full' src={LoginImage} alt="Yummy welcome image" width={120} height={40} />
                </div>
            </div>
            <div className='w-1/2 flex items-center justify-center'>
                <div className='w-1/2 text-black'>
                    <h3 className='font-semibold text-2xl'>Login Yummy</h3>
                    <p className='font-semibold mb-5'>To order some foods</p>
                    <form onSubmit={handleSubmit}>
                        <InputLabel
                            name="email"
                            label="Your Email"
                            type="email"
                            value={values.email}
                            onChange={handleChange}
                            touched={touched.email}
                            error={errors.email}
                        />
                        <InputLabel
                            name="password"
                            label="Your Password"
                            type="password"
                            value={values.password}
                            onChange={handleChange}
                            touched={touched.password}
                            error={errors.password}
                        />
                        <div className="w-full">
                            <Button
                                label="Login"
                                type="submit"
                                bgColor="#1ABC9C"
                                bgHover="#1ABC9C"
                                color="white"
                                width="w-full"
                                isBlock={true}
                                disabled={isSubmitting}
                            />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default index