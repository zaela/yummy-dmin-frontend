import { BreadCrumb } from "../components";
import Layout from '../layout';

export default function Dashboard() {
  
  return (
    <Layout>
      <div className='md:mx-11'>
        <div className="w-full flex justify-between h-9">
          <BreadCrumb 
            directions={[
              { title:'Dashboard', link:'#'},
            ]}
          />
        </div>
        <div className="flex flex-col">
          <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
          </div>
        </div>
      </div>
    </Layout>
  )
}
