import { RiImageAddLine } from 'react-icons/ri'
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import { BreadCrumb, Button, LoadingTable, TextLabel } from '../../components'
import Layout from '../../layout'

export default function LoadingProduct() {
  return (
      <div className='md:mx-11'>
          <BreadCrumb 
              directions={[
              { title:'Product', link:'/products'},
              { title:<Skeleton count={1}/>, link:'#'},
              ]}
          />
          <div>
            <div className="w-full mt-3 mb-6">
                <p className='text-sm font-semibold text-gray-800 w-1/3 mb-2'>Product Image*</p>
                <div className='w-2/3'>
                    <div className="flex items-center w-full">
                      <section 
                          key={'img'}
                          className={`flex flex-col justify-center items-center w-24 h-24 bg-gray-50 rounded-lg border-2 border-gray-300 border-dashed cursor-pointer`
                      }>
                          <div className="flex flex-col justify-center items-center pt-5 pb-5">
                          <Skeleton count={1} width={80} height={80}/>
                          </div>
                      </section>
                    </div> 
                </div>
            </div>
            <div className="md:w-1/2">
              <div className="mb-6 flex w-full">
                  <label className="text-sm font-medium text-gray-900 w-1/3">Product Name</label>
                  <p className="text-sm font-medium text-gray-900 w-1/3"><Skeleton count={1}/></p>
              </div>
              <div className="mb-6 flex w-full">
                  <label className="text-sm font-medium text-gray-900 w-1/3">Price</label>
                  <p className="text-sm font-medium text-gray-900 w-1/3"><Skeleton count={1}/></p>
              </div>
              <div className="mb-6 flex w-full">
                  <label className="text-sm font-medium text-gray-900 w-1/3">Description</label>
                  <p className="text-sm font-medium text-gray-900 w-1/3"><Skeleton count={1}/></p>
              </div>
              <div className="mb-6 flex w-full">
                  <label className="text-sm font-medium text-gray-900 w-1/3">Variant</label>
                  <p className="text-sm font-medium text-gray-900 w-1/3"><Skeleton count={1}/></p>
              </div>
            </div>
            <div className="w-full">
                <LoadingTable/>
            </div>
            <div className="flex items-start mb-6">
                <div className="flex items-center h-5">
                    <input name="status" type="checkbox" 
                        className="w-4.5 h-4.5 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300"
                        checked={false}
                        readOnly
                    />
                </div>
                <label htmlFor="remember" className="ml-2 text-sm font-medium text-gray-900">Status</label>
            </div>
          </div>
      </div>
  )
}
