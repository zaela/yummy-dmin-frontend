import { useFormik } from "formik";
import { useState } from "react";
import { RiImageAddLine } from 'react-icons/ri';
import { IoIosCloseCircle } from 'react-icons/io';
import Swal from "sweetalert2";
import { BreadCrumb, Button, ButtonChooseFile, InputLabel, ProductVariants, ProtectedRoute, SelectLabel, TextAreaLabel } from "../../components";
import { productAddValidation } from "../../utils/validations/productAddValidation";
import { BASE_URL } from "../../utils/constants";
import Layout from "../../layout";
import { addProduct } from "../../services/productService";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import Cookies from "js-cookie";
import { useQuery } from "react-query";
import { getRestaurant } from "../../services/restaurantService";

function add() {
    const router = useRouter()
    const {loginReducer} = useSelector((state) => state);
    const id = Cookies.get('id')
    const token = Cookies.get('token')
    const [Restaurants, setRestaurants] = useState([])
    const [Product, setProduct] = useState({
        product_images:[],
        name:'',
        price:'',
        description:'',
        restaurant_id:id,
        hasVariant:false,
        status:false,
        variants:[],
        variant_images:[],
    })

    const config = {
        headers: { 'Authorization': token },
        data:''
    }

    const uploadFoto = ( index, file, size, image_url) => {
        console.log('upload foto:', file)
        if (file) {
          if (size > 2200000) {
            Swal.fire({
              icon: 'warning',
              title: 'Upload foto Gagal',
              text: 'Maksimal ukuran file 2 Mb !',
              customClass: {
                confirmButton: 'bg-[#1ABC9C] text-white',
              }
            })
    
          } else {
            // let formData = new FormData()
            // formData.append('image', file)
            let images = [...values.product_images]
            images.push({file:file, image_url:image_url})
            setFieldValue('product_images', images)
          }
        } else {
          Swal.fire({
            icon: 'warning',
            title: 'Upload foto Gagal',
            text: 'Silahkan upload kembali foto!'
          })
        }
    }

    const deleteProductPicture = (index) => {
        let data = [...values.product_images]
        data.splice(index, 1)
        setFieldValue('product_images', data)
    }

    const handleHasVariats = (checked) => {
        const variant = [{variant_name:''}]
        const variant_images = [{file:'', image_url:''}]

        checked ? setFieldValue('variants', variant) : setFieldValue('variants', [])
        checked ? setFieldValue('variant_images', variant_images) : setFieldValue('variant_images', [])
    }

    const addVariants = () => {
        let variant = [...values.variants]
        let variant_images = [...values.variant_images]
        variant.push({variant_name:''})
        variant_images.push({file:'', image_url:''})
        setFieldValue('variants', variant)
        setFieldValue('variant_images', variant_images)
    }

    const deleteVariants = (index) => {
        let variants = [...values.variants]
        let variant_images = [...values.variant_images]
        variants.splice(index, 1)
        variant_images.splice(index, 1)
        setFieldValue('variants', variants)
        setFieldValue('variant_images', variant_images)
    }

    const handleVariantImages = (index, e) => {
        const file = e.target.files[0]
        const image_url = URL.createObjectURL(e.target.files[0])
        
        let variant_images = [...values.variant_images]
        variant_images[index].file = file
        variant_images[index].image_url = image_url
        setFieldValue('variant_images', variant_images)
    }

    const handleVariants = (index, value) => {
        let variants = [...values.variants]
        variants[index].variant_name = value
        setFieldValue('variants', variants)
    }

    const onSubmit = async () => {
        let formData = new FormData()

        for (let i = 0; i < values.product_images.length; i++) {
            formData.append(`product_images`, values.product_images[i].file);
        }
        formData.append("name", values.name);
        formData.append("price", values.price);
        formData.append("description", values.description);
        formData.append("restaurant_id", values.restaurant_id);
        formData.append("status", values.status);
        for (let i = 0; i < values.variants.length; i++) {
            formData.append(`variants`, values.variants[i].variant_name);
        }
        for (let i = 0; i < values.variant_images.length; i++) {
            formData.append(`variant_images`, values.variant_images[i].file);
        }

        const response = await addProduct({headers: { 'Authorization': token }, data:formData})
        if (response.status) {
            router.push('/products')
        }
    }

    useQuery(
        ['restaurants'],
        () => getRestaurant({headers:{Authorization:token}}),
        {
          onSuccess: (restaurants) => {
            console.log('restaurants :', restaurants.data)
            restaurants = restaurants.data.map((restaurant) => {
                return {
                    value:restaurant.id,
                    label:restaurant.name,
                }
            }) 
            setRestaurants(restaurants)
          },
        }
      )

    const { values, errors, touched, isSubmitting, handleBlur, handleChange, handleSubmit, handleReset, setFieldValue} = useFormik({
        initialValues:Product,
        validationSchema:productAddValidation,
        onSubmit,
    })

    console.log('values :', values)

    return (
        <Layout>
            <div className='md:mx-11'>
                <BreadCrumb 
                    directions={[
                    { title:'Add Product', link:'#'},
                    ]}
                />
                <div>
                    <form onSubmit={handleSubmit} onReset={handleReset}>
                    {/* label, name, placeholder, type, value, required */}
                        <div className="w-full mt-3 mb-6">
                            <p className='text-sm font-semibold text-gray-800 w-1/3 mb-2'>Product Image*</p>
                            <div className='w-2/3'>
                                <div className="flex items-center w-full">
                                    {
                                        values.product_images.length === 0 && (
                                            <section 
                                                key={'img'}
                                                className={`flex flex-col justify-center items-center w-24 h-24 bg-gray-50 rounded-lg border-2 border-gray-300 border-dashed cursor-pointer`
                                            }>
                                                <div className="flex flex-col justify-center items-center pt-5 pb-5">
                                                    <RiImageAddLine className="mb-0 w-8 h-8 text-gray-400"/>
                                                    <p className="mb-2 text-xs text-gray-500 text-center"><span className="font-semibold">Upload File <br/> 5mb .jpg .png .jpeg</span></p>
                                                </div>
                                            </section>
                                        )
                                    }
                                    {
                                        values.product_images.length > 0 && (
                                            <>
                                            {
                                                values.product_images.map((item, i) => {
                                                    return(
                                                        <>
                                                            
                                                            <section 
                                                                key={i}    
                                                                className={
                                                                    i > 0 ?
                                                                    `relative ml-2 items-center w-24 h-24 bg-gray-50 rounded-lg border-2 border-gray-300 cursor-pointer`
                                                                    :`relative items-center w-24 h-24 bg-gray-50 rounded-lg border-2 border-gray-300 cursor-pointer`
                                                            }>
                                                                    <IoIosCloseCircle color='red' className='h-5 w-5 absolute top-[.1rem] right-[.1rem]' 
                                                                        onClick={() => {deleteProductPicture(i)}}
                                                                    />
                                                                    <div className="w-full max-h-24 justify-center items-center">
                                                                        <img className='w-full max-h-24 rounded-lg' src={item.image_url}/>
                                                                    </div>
                                                            </section>
                                                        </>
                                                    )
                                                })
                                            }
                                            </>
                                        )
                                    }
                                    {
                                        values.product_images.length < 10 && (
                                            //untuk membatasi hanya 5 foto
                                            <ButtonChooseFile
                                                uploadfoto={(index, file, size, image_url) => uploadFoto(index, file, size, image_url)}
                                            />
                                        )
                                    }
                                    <p className='text-sm text-gray-500 ml-1'>({values.product_images.length}/5)</p>
                                </div> 
                                {/* product_images */}
                                {
                                    errors.product_images && touched.product_images && <p className='text-xs mt-1 w-full text-red-600'>{errors.product_images}</p>
                                }
                            </div>
                        </div>
                        <InputLabel
                            name="name"
                            label="Product Name"
                            placeholder="Product Name"
                            type="text"
                            value={values.name}
                            onChange={handleChange}
                            touched={touched.name}
                            error={errors.name}
                        />
                        <SelectLabel
                            name="restaurant_id"
                            label="Restaurant"
                            data={Restaurants}
                            value={values.restaurant_id}
                            onChange={handleChange}
                            touched={touched.restaurant_id}
                            error={errors.restaurant_id}
                        />
                        <InputLabel
                            name="price"
                            label="Price"
                            placeholder="ex 10.000"
                            type="number"
                            value={values.price}
                            onChange={handleChange}
                            touched={touched.price}
                            error={errors.price}
                        />
                        <TextAreaLabel
                            name="description"
                            label="description"
                            placeholder="Desc"
                            type="Text"
                            value={values.description}
                            onChange={handleChange}
                            touched={touched.description}
                            error={errors.description}
                        />
                        <div className="flex items-start mb-3">
                            <div className="flex items-center h-5">
                                <input name="hasVariant" type="checkbox" 
                                    value={values.hasVariant} 
                                    checked={values.hasVariant}
                                    className="w-4.5 h-4.5 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300"
                                    onChange={(e) => {
                                        handleChange(e) 
                                        handleHasVariats(e.target.checked)
                                    }} 
                                />
                            </div>
                            <label htmlFor="remember" className="ml-2 text-sm font-medium text-gray-900">Variant</label>
                        </div>
                        {
                            values.hasVariant && (
                                <div className="w-full">
                                    <ProductVariants
                                        variants={values.variants}
                                        variantImages={values.variant_images}
                                        handleVariantImages={(index, file, image_url) => handleVariantImages(index, file, image_url)}
                                        handleVariants={(index, value) => handleVariants(index, value)}
                                        addVariants={(variant) => addVariants(variant)}
                                        deleteVariants={(index) => deleteVariants(index)}
                                    />
                                </div>
                            )
                        }
                        <div className="flex items-start mb-6">
                            <div className="flex items-center h-5">
                                <input name="status" type="checkbox" value={values.status} 
                                    onChange={handleChange} 
                                    className="w-4.5 h-4.5 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300"
                                    checked={values.status}
                                />
                            </div>
                            <label htmlFor="remember" className="ml-2 text-sm font-medium text-gray-900">Status</label>
                        </div>
                        {/* label, type, bgColor, bgHover, color, colorHover, onClick */}
                        <div className="flex gap-2">
                            <Button
                                label="Reset"
                                type="reset"
                                bgColor="#D9435E"
                                bgHover="#D9435E"
                                color="white"
                            />
                            <Button
                                label="Submit"
                                type="submit"
                                bgColor="#1ABC9C"
                                bgHover="#1ABC9C"
                                color="white"
                                disabled={isSubmitting}
                            />
                        </div>
                    </form>
                </div>
            </div>
        </Layout>
    )
}

export default ProtectedRoute(add)
