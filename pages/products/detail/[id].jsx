import Image from "next/image";
import { useRouter } from "next/router";
import { useState } from "react";
import { RiImageAddLine } from 'react-icons/ri';
import { useQuery } from "react-query";
import { useSelector } from "react-redux";
import { BreadCrumb, Button, ProtectedRoute, TextLabel } from "../../../components";
import ProductVariantsDetail from "../../../components/Pages/products/ProductVariantsDetail";
import Layout from "../../../layout";
import { detailProduct } from "../../../services/productService";
import { BASE_URL } from "../../../utils/constants";
import LoadingProduct from "../LoadingProduct";

function detail() {
    const router = useRouter()
    const {id} = router.query;
    console.log('router.query :', id)
    const {loginReducer} = useSelector((state) => state);
    const {token} = loginReducer
    const [Product, setProduct] = useState({
        product_images:[],
        name:'',
        price:'',
        description:'',
        hasVariant:false,
        status:false,
        variants:[],
        variant_images:[],
    })

    const config = {
        headers: { 'Authorization': token },
        params:{id:id}
    }
    
    const { isLoading, isError, error } = useQuery(
        ['product'],
        () => detailProduct(config),
        {
            onSuccess: (product) => {
                setProduct(product.data)
            },
        }
    )

    return (
        <Layout>
            {
                isLoading ?
                <LoadingProduct/>
                :
                <div className='md:mx-11'>
                    <BreadCrumb 
                        directions={[
                        { title:'Product', link:'/products'},
                        { title:Product?.name, link:'#'},
                        ]}
                    />
                    <div>
                        {/* label, name, placeholder, type, value, required */}
                            <div className="w-full mt-3 mb-6">
                                <p className='text-sm font-semibold text-gray-800 w-1/3 mb-2'>Product Image*</p>
                                <div className='w-2/3'>
                                    <div className="flex items-center w-full">
                                        {
                                            Product?.product_images?.length === 0 && (
                                                <section 
                                                    key={'img'}
                                                    className={`flex flex-col justify-center items-center w-24 h-24 bg-gray-50 rounded-lg border-2 border-gray-300 border-dashed cursor-pointer`
                                                }>
                                                    <div className="flex flex-col justify-center items-center pt-5 pb-5">
                                                        <RiImageAddLine className="mb-0 w-8 h-8 text-gray-400"/>
                                                        <p className="mb-2 text-xs text-gray-500 text-center"><span className="font-semibold">Upload File <br/> 5mb .jpg .png .jpeg</span></p>
                                                    </div>
                                                </section>
                                            )
                                        }
                                        {
                                            Product?.product_images?.length > 0 && (
                                                <>
                                                {
                                                    Product?.product_images?.map((item, i) => {
                                                        return(
                                                            <>
                                                                
                                                                <section 
                                                                    key={i}    
                                                                    className={
                                                                        i > 0 ?
                                                                        `relative ml-2 items-center w-24 h-24 bg-gray-50 rounded-lg border-2 border-gray-300 cursor-pointer`
                                                                        :`relative items-center w-24 h-24 bg-gray-50 rounded-lg border-2 border-gray-300 cursor-pointer`
                                                                }>
                                                                        <div className="w-full max-h-24 justify-center items-center">
                                                                            <Image width={100} height={100} alt="product image" className='w-full max-h-24 rounded-lg' src={`${BASE_URL}${item.file_name}`}/>
                                                                        </div>
                                                                </section>
                                                            </>
                                                        )
                                                    })
                                                }
                                                </>
                                            )
                                        }
                                    </div> 
                                </div>
                            </div>
                            <div className="md:w-1/2">
                                <TextLabel
                                    name="name"
                                    label="Product Name"
                                    placeholder="Product Name"
                                    type="text"
                                    value={Product?.name}
                                />
                                <TextLabel
                                    name="price"
                                    label="Price"
                                    placeholder="ex 10.000"
                                    type="number"
                                    value={Product?.price}
                                />
                                <TextLabel
                                    name="description"
                                    label="description"
                                    placeholder="Desc"
                                    type="Text"
                                    value={Product?.description}
                                />
                            </div>
                            <div className="flex items-start mb-3">
                                <div className="flex items-center h-5">
                                    <input name="hasVariant" type="checkbox" 
                                        checked={Product?.product_variants?.length > 0 ? true : false}
                                        readOnly
                                        className="w-4.5 h-4.5 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300"
                                    />
                                </div>
                                <label htmlFor="remember" className="ml-2 text-sm font-medium text-gray-900">Variant</label>
                            </div>
                            {
                                Product?.product_variants && (
                                    <div className="w-full">
                                        <ProductVariantsDetail
                                            variants={Product?.product_variants}
                                        />
                                    </div>
                                )
                            }
                            <div className="flex items-start mb-6">
                                <div className="flex items-center h-5">
                                    <input name="status" type="checkbox" 
                                        className="w-4.5 h-4.5 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300"
                                        checked={Product?.status}
                                        readOnly
                                    />
                                </div>
                                <label htmlFor="remember" className="ml-2 text-sm font-medium text-gray-900">Status</label>
                            </div>
                            {/* label, type, bgColor, bgHover, color, colorHover, onClick */}
                            <div className="flex">
                                <Button
                                    label="Edit"
                                    type="butoon"
                                    bgColor="#1ABC9C"
                                    bgHover="#1ABC9C"
                                    color="white"
                                    onClick={() => router.push(`/products/edit/${id}`)}
                                />
                            </div>
                    </div>
                </div>
            }
        </Layout>
    )
}

export default ProtectedRoute(detail)