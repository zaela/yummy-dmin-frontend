import Cookies from "js-cookie";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { MdAdd } from 'react-icons/md';
import { useQuery } from "react-query";
import { ActionButton, BreadCrumb, Button, LoadingTable, ProtectedRoute } from "../../components";
import PaginationComponentTransaction from "../../components/Pagination";
import Layout from "../../layout";
import { deleteProduct, getProduct } from "../../services/productService";
import { BASE_URL } from "../../utils/constants";
import currencyConverter from "../../utils/helpers/formatter";
import socket from "../../utils/socket";

function Index() {
  const router = useRouter()
  // const [products, setProducts] = useState([])
  const [totalPage, setTotalPage] = useState(1)
  const [page, setPage] = useState(1)
  const [search, setSearch] = useState(null)
  const token = Cookies.get('token');
  const config = {headers:{Authorization:token}, params:{limit:3, offset:1}}

  const { isLoading, data:products, refetch, isError, error } = useQuery(
    ['products', page, search],
    () => getProduct({headers:{Authorization:token}, params:{limit:3, offset:page, name:search}})
    // {
    //   onSuccess: (products) => {
    //     setProducts(products)
    //   },
    // }
  )

  console.log("products :", products)

  socket.on('order', (data) => {
    console.log('order :', data)
  });

  const handleClick = async (action, id) => {
    switch (action) {
      case 'detail':
        router.push(`/products/detail/${id}`)
        break;
      case 'edit':
        router.push(`/products/edit/${id}`)
        break;
      case 'delete':
        await deleteProduct({headers:{Authorization:token}, params:{id:id}})
        refetch();
        break;
      default:
        break;
    }
  }

  return (
    <>
      {
        isLoading ? (
          <Layout>
            <div className="w-full flex justify-between h-9">
                <BreadCrumb 
                  directions={[
                    { title:'Productss', link:'#'},
                  ]}
                />
      
                <Link href="/products/add">
                  <Button
                      label="Add"
                      type="button"
                      bgColor="#1ABC9C"
                      bgHover="#1ABC9C"
                      color="white"
                      icon={<MdAdd className="h-5 w-5 font-semibold text-white"/>}
                  />
                </Link>
              </div>
            <LoadingTable/>
          </Layout>
        ): (
          <Layout>
            <div className='md:mx-11'>
              <div className="w-full flex justify-between h-9">
                <BreadCrumb 
                  directions={[
                    { title:'Productss', link:'#'},
                  ]}
                />
      
                <Link href="/products/add">
                  <Button
                      label="Add"
                      type="button"
                      bgColor="#1ABC9C"
                      bgHover="#1ABC9C"
                      color="white"
                      icon={<MdAdd className="h-5 w-5 font-semibold text-white"/>}
                  />
                </Link>
              </div>
              <div className="flex flex-col">
                <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
                  <div className="py-4 inline-block min-w-full sm:px-6 lg:px-8">
                    <div className="overflow-hidden">
                      <table className="min-w-full text-center">
                        <thead className="border-b bg-[#FFC700]">
                          <tr>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              No
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Image
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Product Name
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Price
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Rating
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Status
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Action
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            products.status && products.data.length > 0 ? (
                              products.data.map((product, index) => (
                                <tr className="bg-white border-b hover:bg-gray-200">
                                  <td className="px-4 py-3 text-sm text-gray-900">{index+1}</td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    <Image width={80} height={80} alt="product image" src={`${BASE_URL}${product?.product_images[0]?.file_name}`}/>
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    {product.name}
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    {currencyConverter(product.price)}
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    {'-'}
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    {product.status? 'Active' : 'Inactive'}
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    <ActionButton id={product.id} onClick={(action, id) => handleClick(action, id)}/>
                                  </td>
                                </tr>
                              ))
                            ) :
                            <tr className="bg-white border-b hover:bg-gray-200">
                              <td className="px-4 py-3 text-sm text-gray-900" colSpan={6}>Data empty</td>
                            </tr>
                          }
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

              <div className="w-full flex justify-center">
                <PaginationComponentTransaction
                  page={products?.page ?? 0}
                  between={3}
                  total={products?.total ?? 0}
                  limit={3}
                  changePage={(page) => {
                    setPage(page)
                    refetch()
                  }}
                  ellipsis={1}
                />
              </div>
            </div>
          </Layout>
        )
      }
    </>
  )
}

export default ProtectedRoute(Index)

// export async function getStaticProps() {
//   const products = await getProduct({params:{limit:10, offset:1}})
//   return {
//     props: {
//       products,
//     },
//   }
// }
