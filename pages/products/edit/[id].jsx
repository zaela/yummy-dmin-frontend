import { useFormik } from "formik";
import { useEffect, useState } from "react";
import { RiImageAddLine } from 'react-icons/ri';
import { IoIosCloseCircle } from 'react-icons/io';
import Swal from "sweetalert2";
import { BreadCrumb, Button, ButtonChooseFile, InputLabel, ProductVariants, ProtectedRoute, TextAreaLabel } from "../../../components";
import { productAddValidation } from "../../../utils/validations/productAddValidation";
import { BASE_URL } from "../../../utils/constants";
import Layout from "../../../layout";
import { addProduct, detailProduct, editProduct } from "../../../services/productService";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import Image from "next/image";

function edit() {
    const router = useRouter()
    const {id} = router.query;
    const {loginReducer} = useSelector((state) => state);
    const {token} = loginReducer
    const [Product, setProduct] = useState({
        product_id:'',
        product_images:[],
        product_images_new:[],
        product_images_delete:[],
        name:'',
        price:'',
        description:'',
        hasVariant:false,
        status:false,
        variants:[],
        variant_images:[],
        variant_images_new:[],
        variants_delete:[],
    })

    const uploadFoto = ( index, file, size, image_url) => {
        console.log('upload foto:', file)
        if (file) {
          if (size > 2200000) {
            Swal.fire({
              icon: 'warning',
              title: 'Upload foto Gagal',
              text: 'Maksimal ukuran file 2 Mb !',
              customClass: {
                confirmButton: 'bg-[#1ABC9C] text-white',
              }
            })
    
          } else {
            let images = [...values.product_images]
            let images_new = [...values.product_images_new]
            images.push({id:'', file:file, file_name:image_url})
            images_new.push({id:'', file:file, file_name:image_url})
            setFieldValue('product_images', images)
            setFieldValue('product_images_new', images_new)
          }
        } else {
          Swal.fire({
            icon: 'warning',
            title: 'Upload foto Gagal',
            text: 'Silahkan upload kembali foto!'
          })
        }
    }

    const deleteProductPicture = (index, picture) => {
        const {id} = picture
        let data = [...values.product_images]
        data.splice(index, 1)
        setFieldValue('product_images', data)

        if (id !== '') { // is delete existing image
            let images_delete = [...values.product_images_delete]
            images_delete.push(id)
            setFieldValue('product_images_delete', images_delete)
        }else{
            let images_new = [...values.product_images_new]
            const index = images_new.findIndex(e => e === picture)
            images_new.splice(index, 1)
            setFieldValue('product_images_new', images_new)
        }


    }

    const handleHasVariants = (checked) => {
        const variant = [{id:'', variant_name:''}]
        const variant_images = [{id:'', file:'', image_url:''}]

        checked ? setFieldValue('variants', variant) : setFieldValue('variants', [])
        checked ? setFieldValue('variant_images', variant_images) : setFieldValue('variant_images', [])
        checked ? setFieldValue('variant_images_new', variant_images) : setFieldValue('variant_images_new', [])
    }

    const addVariants = () => {
        let variant = [...values.variants]
        let variant_images = [...values.variant_images]
        let variant_images_new = [...values.variant_images_new]
        variant.push({id:'', variant_name:''})
        variant_images.push({id:'', file:'', image_url:''})
        variant_images_new.push({id:'', file:'', image_url:''})
        setFieldValue('variants', variant)
        setFieldValue('variant_images', variant_images)
        setFieldValue('variant_images_new', variant_images_new)
    }

    const deleteVariants = (index, variant) => {
        let variants = [...values.variants]
        let variant_images = [...values.variant_images]
        variants.splice(index, 1)
        variant_images.splice(index, 1)
        setFieldValue('variants', variants)
        setFieldValue('variant_images', variant_images)
        
        if (variant.id !== '') {
            let variants_delete = [...values.variants_delete]
            variants_delete.push(variant.id)
            setFieldValue('variants_delete', variants_delete)
        } else {
            let images_new = [...values.variant_images_new]
            const index = images_new.findIndex(e => e === picture)
            images_new.splice(index, 1)
            setFieldValue('variant_images_new', images_new)
        }
    }

    const handleVariantImages = (index, e) => {
        const file = e.target.files[0]
        const image_url = URL.createObjectURL(e.target.files[0])

        let variant_images = [...values.variant_images]
        variant_images[index].file = file
        variant_images[index].image_url = image_url
        setFieldValue('variant_images', variant_images)
    }

    const handleVariants = (index, value) => {
        let variants = [...values.variants]
        variants[index].variant_name = value
        setFieldValue('variants', variants)
    }

    const config = {
        headers: { 'Authorization': token },
        params:{id:id}
    }
    const getDetail = async () => {
        const product = await detailProduct(config)
        if (product.status) {
            const product_images = product?.data?.product_images.map((product_image) => {
                return {
                    ...product_image,
                    file:'',
                }
            })
            const variants = product?.data?.product_variants.map((variant) => {
                return {
                    id:variant.id,
                    variant_name:variant.variant_name,
                }
            })
            const variant_images = product?.data?.product_variants.map((variant) => {
                return {
                    id:variant.id,
                    file:'',
                    image_url:variant.variant_image,
                }
            })
            setProduct({
                ...product.data,
                product_id:id,
                product_images:product_images,
                product_images_new:[],
                product_images_delete:[],
                hasVariant:variants.length > 0 ? true : false,
                variants:variants,
                variants_delete:[],
                variant_images:variant_images,
                variant_images_new:[],
            })
        }
    }
    useEffect(() => {
        if (id) {
            getDetail()
        }
    }, [id])

    const onSubmit = async () => {
        let formData = new FormData()

        formData.append("product_id", values.product_id);
        for (let i = 0; i < values.product_images_new.length; i++) {
            formData.append(`product_images_new`, values.product_images_new[i].file);
        }
        for (let i = 0; i < values.product_images_delete.length; i++) {
            formData.append(`product_images_delete`, values.product_images_delete[i]);
        }
        formData.append("name", values.name);
        formData.append("price", values.price);
        formData.append("description", values.description);
        formData.append("status", values.status);
        
        if (values.variants.length) {
            formData.append(`variants`, JSON.stringify(values.variants));
        }

        if (values.variants_delete.length) {
            formData.append(`variants_delete`, JSON.stringify(values.variants_delete));
        }

        // pakai json stringify trus dari belakang  update gambarnya dipisahin trus dipisahin juga varian image yang lama sama yang baru
        // for (let i = 0; i < values.variant_images.length; i++) {
        //     formData.append(`variant_images`, values.variant_images[i].file);
        //     if (values.variant_images[i].file) {
        //         formData.append(`variant_images_id`, values.variant_images[i].id);
        //     }
        // }

        if (values.variant_images.length) {
            values.variant_images.filter((item) => item.id !== "" && item.file !== "").map((item) => formData.append(`variant_images`, item.file))
            values.variant_images.filter((item) => item.id !== "" && item.file !== "").map((item) => formData.append(`variant_images_id`, item.id))
    
            values.variant_images.filter((item) => item.id === "").map((item) => formData.append(`variant_images_new`, item.file))
            values.variant_images.filter((item) => item.id === "").map((item) => formData.append(`variant_images_id_new`, item.id))
        }

        const response = await editProduct({headers: { 'Authorization': token }, data:formData})

        if (response.status) {
            router.push('/products')
        }
    }

    const { values, errors, touched, isSubmitting, handleBlur, handleChange, handleSubmit, handleReset, setFieldValue} = useFormik({
        initialValues:Product,
        enableReinitialize:true,
        validationSchema:productAddValidation,
        onSubmit,
    })

    console.log('values :', values)

    return (
        <Layout>
            <div className='md:mx-11'>
                <BreadCrumb 
                    directions={[
                    { title:'Product', link:'/products'},
                    { title:Product.name, link:'#'},
                    ]}
                />
                <div>
                    <form onSubmit={handleSubmit} onReset={handleReset}>
                    {/* label, name, placeholder, type, value, required */}
                        <div className="w-full mt-3 mb-6">
                            <p className='text-sm font-semibold text-gray-800 w-1/3 mb-2'>Product Image*</p>
                            <div className='w-2/3'>
                                <div className="flex items-center w-full">
                                    {
                                        values.product_images.length === 0 && (
                                            <section 
                                                key={'img'}
                                                className={`flex flex-col justify-center items-center w-24 h-24 bg-gray-50 rounded-lg border-2 border-gray-300 border-dashed cursor-pointer`
                                            }>
                                                <div className="flex flex-col justify-center items-center pt-5 pb-5">
                                                    <RiImageAddLine className="mb-0 w-8 h-8 text-gray-400"/>
                                                    <p className="mb-2 text-xs text-gray-500 text-center"><span className="font-semibold">Upload File <br/> 5mb .jpg .png .jpeg</span></p>
                                                </div>
                                            </section>
                                        )
                                    }
                                    {
                                        values.product_images.length > 0 && (
                                            <>
                                            {
                                                values.product_images.map((item, i) => {
                                                    return(
                                                        <>
                                                            
                                                            <section 
                                                                key={i}    
                                                                className={
                                                                    i > 0 ?
                                                                    `relative ml-2 items-center w-24 h-24 bg-gray-50 rounded-lg border-2 border-gray-300 cursor-pointer`
                                                                    :`relative items-center w-24 h-24 bg-gray-50 rounded-lg border-2 border-gray-300 cursor-pointer`
                                                            }>
                                                                    <IoIosCloseCircle color='red' className='h-5 w-5 absolute top-[.1rem] right-[.1rem]' 
                                                                        onClick={() => {deleteProductPicture(i, item)}}
                                                                    />
                                                                    <div className="w-full max-h-24 justify-center items-center">
                                                                        <Image width={100} height={100} className='w-full max-h-24 rounded-lg' alt="product picture"
                                                                        src={ item.file === '' ? `${BASE_URL}${item.file_name}` : `${item.file_name}`}/>
                                                                    </div>
                                                            </section>
                                                        </>
                                                    )
                                                })
                                            }
                                            </>
                                        )
                                    }
                                    {
                                        values.product_images.length < 10 && (
                                            //untuk membatasi hanya 5 foto
                                            <ButtonChooseFile
                                                uploadfoto={(index, file, size, image_url) => uploadFoto(index, file, size, image_url)}
                                            />
                                        )
                                    }
                                    <p className='text-sm text-gray-500 ml-1'>({values.product_images.length}/10)</p>
                                </div> 
                                {/* product_images */}
                                {
                                    errors.product_images && touched.product_images && <p className='text-xs mt-1 w-full text-red-600'>{errors.product_images}</p>
                                }
                            </div>
                        </div>
                        <InputLabel
                            name="name"
                            label="Product Name"
                            placeholder="Product Name"
                            type="text"
                            value={values.name}
                            onChange={handleChange}
                        />
                        <InputLabel
                            name="price"
                            label="Price"
                            placeholder="ex 10.000"
                            type="number"
                            value={values.price}
                            onChange={handleChange}
                        />
                        <TextAreaLabel
                            name="description"
                            label="description"
                            placeholder="Desc"
                            type="Text"
                            value={values.description}
                            onChange={handleChange}
                        />
                        <div className="flex items-start mb-2">
                            <div className="flex items-center h-5">
                                <input name="hasVariant" type="checkbox" 
                                    value={values.hasVariant} 
                                    checked={values.hasVariant}
                                    className="w-4.5 h-4.5 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300"
                                    onChange={(e) => {
                                        handleChange(e) 
                                        handleHasVariants(e.target.checked)
                                    }} 
                                />
                            </div>
                            <label htmlFor="remember" className="ml-2 text-sm font-medium text-gray-900">Variant</label>
                        </div>
                        {
                            values?.variants?.length > 0 && (
                                <div className="w-full">
                                    <ProductVariants
                                        variants={values.variants}
                                        variantImages={values.variant_images}
                                        handleVariantImages={(index, e) => handleVariantImages(index, e)}
                                        handleVariants={(index, value) => handleVariants(index, value)}
                                        addVariants={(variant) => addVariants(variant)}
                                        deleteVariants={(index, variant) => deleteVariants(index, variant)}
                                    />
                                </div>
                            )
                        }
                        <div className="flex items-start mb-6">
                            <div className="flex items-center h-5">
                                <input name="status" type="checkbox" value={values.status} 
                                    onChange={handleChange} 
                                    className="w-4.5 h-4.5 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300"
                                    checked={values.status}
                                />
                            </div>
                            <label htmlFor="remember" className="ml-2 text-sm font-medium text-gray-900">Status</label>
                        </div>
                        {/* label, type, bgColor, bgHover, color, colorHover, onClick */}
                        <div className="flex gap-2">
                            <Button
                                label="Reset"
                                type="reset"
                                bgColor="#D9435E"
                                bgHover="#D9435E"
                                color="white"
                            />
                            <Button
                                label="Submit"
                                type="submit"
                                bgColor="#1ABC9C"
                                bgHover="#1ABC9C"
                                color="white"
                                disabled={isSubmitting}
                            />
                        </div>
                    </form>
                </div>
            </div>
        </Layout>
    )
}

export default ProtectedRoute(edit)
