import Cookies from "js-cookie";
import { useRouter } from "next/router";
import { useState } from "react";
import { useQuery } from "react-query";
import { ActionButtonTr, BreadCrumb, LoadingTable, ProtectedRoute } from "../../components";
import Layout from "../../layout";
import { cancelTransaction, completeTransaction, deleteTransaction, getTransaction } from "../../services/transactionService";
import { formatCurrency } from "../../utils/helpers/formatter";
import socket from "../../utils/socket";

function Index() {
  const router = useRouter()
  const [transactions, setTransactions] = useState([])
  const [totalPage, setTotalPage] = useState(1)
  const [page, setPage] = useState(1)
  const [search, setSearch] = useState(null)
  const token = Cookies.get('token');

  const { isLoading, isError, error } = useQuery(
    ['transactions', page, search],
    () => getTransaction({headers:{Authorization:token}, params:{limit:10, offset:page, name:search}}),
    {
      onSuccess: (transactions) => {
        setTransactions(transactions)
        socket.emit('order', 'New order comes');
      },
    }
  )

  // console.log('transactions :', transactions)
  socket.on('message', (data) => {
    console.log('message :', data)
  });
  socket.on('order', (data) => {
    console.log('order :', data)
  });

  const order = () => {
    socket.emit('order', 'New order comes');
  }

  const cancel = async (id) => {
    await cancelTransaction({headers:{Authorization:token}, data:{id:id, status:'CANCELLED'}})
    await getTransaction({headers:{Authorization:token}, params:{limit:10, offset:page, name:search}}).then((result) => {
      setTransactions(result)
    })
  }

  const complete = async (id) => {
    await completeTransaction({headers:{Authorization:token}, data:{id:id, status:'COMPLETED'}})
    await getTransaction({headers:{Authorization:token}, params:{limit:10, offset:page, name:search}}).then((result) => {
      setTransactions(result)
    })
  }

  const handleDelete = async (id) => {
    await deleteTransaction({headers:{Authorization:token}, data:{id:id}})
    await getTransaction({headers:{Authorization:token}, params:{limit:10, offset:page, name:search}}).then((result) => {
      setTransactions(result)
    })
  }

  const handleClick = async (action, id) => {
    switch (action) {
      case 'cancel':
        await cancel(id)
        break;
      case 'complete':
        await complete(id)
        break;
      case 'delete':
        await handleDelete(id)
        break;
      default:
        break;
    }
  }
  

  return (
    <>
      {
        isLoading ? (
          <Layout>
            <div className="w-full flex justify-between h-9">
                <BreadCrumb 
                  directions={[
                    { title:'Transactions', link:'#'},
                  ]}
                />
              </div>
            <LoadingTable/>
          </Layout>
        ): (
          <Layout>
            <div className='md:mx-11'>
              <div className="w-full flex justify-between h-9">
                <BreadCrumb 
                  directions={[
                    { title:'Transactions', link:'#'},
                  ]}
                />
      
              </div>
              {/* <button className="bg-green-300 text-sm text-white" onClick={() => order()}>order</button> */}
              <div className="flex flex-col">
                <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
                  <div className="py-4 inline-block min-w-full sm:px-6 lg:px-8">
                    <div className="overflow-hidden">
                      <table className="min-w-full text-center">
                        <thead className="border-b bg-[#FFC700]">
                          <tr>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              No
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              No order
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Product Name
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Quantity
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Price
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Rating
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Status
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Action
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            transactions.status && transactions.data.length > 0 ? (
                              transactions.data.map((transaction, index) => (
                                <tr className="bg-white border-b hover:bg-gray-200">
                                  <td className="px-4 py-3 text-sm text-gray-900">{index+1}</td>
                                  <td className="px-4 py-3 text-sm text-gray-900">{transaction?.id}</td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    {transaction?.product?.name}
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    {transaction.qty}
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    {formatCurrency(Number(transaction?.product?.price))}
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    {formatCurrency(transaction?.product?.price * transaction?.qty)}
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    {transaction?.status}
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    <ActionButtonTr data={transaction} onClick={(action, id) => handleClick(action, id)}/>
                                  </td>
                                </tr>
                              ))
                            ) :
                            <tr className="bg-white border-b hover:bg-gray-200">
                              <td className="px-4 py-3 text-sm text-gray-900" colSpan={6}>Data empty</td>
                            </tr>
                          }
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Layout>
        )
      }
    </>
  )
}

export default ProtectedRoute(Index)

// export async function getStaticProps() {
//   const transactions = await getTransaction({params:{limit:10, offset:1}})
//   return {
//     props: {
//       transactions,
//     },
//   }
// }
