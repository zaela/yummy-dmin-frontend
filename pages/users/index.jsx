import Cookies from "js-cookie";
import Image from "next/image";
import { useRouter } from "next/router";
import { useState } from "react";
import { RiImageAddLine } from "react-icons/ri";
import { useQuery, useQueryClient } from "react-query";
import { ActionButton, BreadCrumb, LoadingTable, ProtectedRoute } from "../../components";
import Layout from "../../layout";
import { getUser } from "../../services/authService";
import { BASE_URL } from "../../utils/constants";

function Index() {
  const router = useRouter()
  const [users, setUsers] = useState([])
  const [totalPage, setTotalPage] = useState(1)
  const [page, setPage] = useState(1)
  const [search, setSearch] = useState(null)
  const token = Cookies.get('token');
  const queryClient = useQueryClient()

  const { isLoading, isError, error } = useQuery(
    ['users', page, search],
    () => getUser({headers:{Authorization:token}, params:{limit:10, offset:page, name:search}}),
    {
      onSuccess: (users) => {
        setUsers(users)
      },
      onError:(error) => {
        console.log(error)
      }
    }
  )

  // const {mutate} = useMutation(`users`, deleteRestaurant, {
  //   onSuccess: () => {
  //     queryClient.invalidateQueries('users')
  //   },
  // })

  const handleMutate = async (config) => {
    mutate(config)
  }

  const handleClick = async (action, id) => {
    switch (action) {
      case 'detail':
        router.push(`/users/detail/${id}`)
        break;
      case 'edit':
        router.push(`/users/edit/${id}`)
        break;
      case 'delete':
        await handleMutate({headers:{Authorization:token}, params:{id:id}})
        break;
      default:
        break;
    }
  }

  return (
    <>
      {
        isLoading ? (
          <Layout>
            <div className="w-full flex justify-between h-9">
                <BreadCrumb 
                  directions={[
                    { title:'Users', link:'#'},
                  ]}
                />
      
              </div>
            <LoadingTable/>
          </Layout>
        ): (
          <Layout>
            <div className='md:mx-11'>
              <div className="w-full flex justify-between h-9">
                <BreadCrumb 
                  directions={[
                    { title:'Users', link:'#'},
                  ]}
                />
              </div>
              <div className="flex flex-col">
                <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
                  <div className="py-4 inline-block min-w-full sm:px-6 lg:px-8">
                    <div className="overflow-hidden">
                      <table className="min-w-full text-center">
                        <thead className="border-b bg-[#FFC700]">
                          <tr>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              No
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3 text-left">
                              User Image
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Email
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Status
                            </th>
                            <th scope="col" className="text-sm text-white px-4 py-3">
                              Action
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            users.status && users.data.length > 0 ? (
                              users.data.map((users, index) => (
                                <tr className="bg-white border-b hover:bg-gray-200">
                                  <td className="px-4 py-3 text-sm text-gray-900">{index+1}</td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    {
                                      users?.profile_picture === null ?
                                      <RiImageAddLine className="w-8 h-8 text-gray-400"/>
                                      :
                                      <Image width={60} height={60} alt="product image" src={`${BASE_URL}${users?.profile_picture}`}/>
                                    }
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    {users?.email}
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    {users?.status? 'Active' : 'Inactive'}
                                  </td>
                                  <td className="text-sm text-gray-900 px-4 py-3">
                                    <ActionButton id={users.id} onClick={(action, id) => handleClick(action, id)}/>
                                  </td>
                                </tr>
                              ))
                            ) :
                            <tr className="bg-white border-b hover:bg-gray-200">
                              <td className="px-4 py-3 text-sm text-gray-900" colSpan={6}>Data empty</td>
                            </tr>
                          }
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Layout>
        )
      }
    </>
  )
}

export default ProtectedRoute(Index)

// export async function getStaticProps() {
//   const users = await getProduct({params:{limit:10, offset:1}})
//   return {
//     props: {
//       users,
//     },
//   }
// }
