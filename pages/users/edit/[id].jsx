import { useFormik } from "formik";
import Cookies from "js-cookie";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useQuery } from "react-query";
import { useSelector } from "react-redux";
import Swal from "sweetalert2";
import { BreadCrumb, Button, CheckboxLabel, ChooseFoto, InputLabel, ProtectedRoute } from "../../../components";
import Layout from "../../../layout";
import { detailUser, editUser } from "../../../services/authService";
import { restaurantAddValidation } from "../../../utils/validations/restaurantAddValidation";
import LoadingProduct from "../../products/LoadingProduct";

function add() {
    const router = useRouter()
    const {id} = router.query;
    const token = Cookies.get('token');
    const id_user = Cookies.get('id');
    const [isLoading, setIsLoading] = useState(true)
    const [User, setUser] = useState({
        restaurant_image:{file:'', image_url:''},
        name:'',
        location:'',
        status:false,
    })

    const config = {
        headers: { 'Authorization': token },
        data:''
    }

    const handleVariantImages = (e) => {
        const file = e.target.files[0]
        const size = e.target.files[0].size
        const image_url = URL.createObjectURL(e.target.files[0])
        console.log('upload foto:', file)
        if (file) {
          if (size > 2200000) {
            Swal.fire({
              icon: 'warning',
              title: 'Upload foto Gagal',
              text: 'Maksimal ukuran file 2 Mb !',
              customClass: {
                confirmButton: 'bg-[#1ABC9C] text-white',
              }
            })
    
          } else {
            // let formData = new FormData()
            // formData.append('image', file)
            setFieldValue('restaurant_image', {file:file, image_url:image_url})
          }
        } else {
          Swal.fire({
            icon: 'warning',
            title: 'Upload foto Gagal',
            text: 'Silahkan upload kembali foto!'
          })
        }
    }

    const onSubmit = async () => {
        let formData = new FormData()

        formData.append("id", id);
        formData.append("user_id", id_user);
        values?.restaurant_image.file && (
            formData.append(`restaurant_image`, values?.restaurant_image.file)
        )
        formData.append("name", values?.name);
        formData.append("location", values?.location);
        formData.append("status", values?.status);

        const response = await editUser({headers: { 'Authorization': token }, data:formData})
        if (response.status) {
            router.push('/restaurants')
        }
    }

    const config1 = {
        headers: { 'Authorization': token },
        // params:{id:id}
        params:{id:id}
    }

    const getDetail = async () => {
        const restaurants = await detailUser(config1)
        console.log('restaurants :', restaurants)
        setIsLoading(false)
        setUser({
            ...restaurants.data,
            restaurant_image:{file:'', image_url:restaurants?.data?.restaurant_image}
        })
    }

    useEffect(() => {
        getDetail()
    },[])

    const { values, errors, touched, isSubmitting, handleBlur, handleChange, handleSubmit, handleReset, setFieldValue} = useFormik({
        initialValues:User,
        enableReinitialize:true,
        validationSchema:restaurantAddValidation,
        onSubmit,
    })

    console.log('values :', values)

    return (
        <Layout>
            <div className='md:mx-11'>
                {
                    isLoading ?
                    <LoadingProduct/>
                    :
                    <>
                    <BreadCrumb 
                        directions={[
                        { title:'Edit User', link:'#'},
                        ]}
                    />
                    <div>
                        <form onSubmit={handleSubmit} onReset={handleReset}>
                        {/* label, name, placeholder, type, value, required */}
                            <div className="w-full mt-3 mb-6">
                                <p className='text-sm font-semibold text-gray-800 w-1/3 mb-2'>User Image*</p>
                                {/* <div className='w-2/3'>
                                    <div className="flex items-center w-full">
                                        <ChooseFoto image={values?.restaurant_image} 
                                            uploadfoto={(e) => { handleVariantImages(e) }}
                                        />
                                    </div> 
                                    {
                                        errors.restaurant_image && touched.restaurant_image && <p className='text-xs mt-1 w-full text-red-600'>{errors.restaurant_image}</p>
                                    }
                                </div> */}
                            </div>
                            <InputLabel
                                name="username"
                                label="Username"
                                placeholder="Username"
                                type="text"
                                value={values?.username}
                                onChange={handleChange}
                            />
                            <InputLabel
                                name="email"
                                label="Email"
                                placeholder="Email"
                                type="text"
                                value={values?.email}
                                onChange={handleChange}
                            />
                            <div className="w-full mt-3 mb-6">
                                <p className='text-sm font-semibold text-gray-800 w-1/3 mb-2'>Role</p>
                            </div>
                            <CheckboxLabel
                                name="role"
                                label="Customer"
                                type="checkbox"
                                value="customer"
                                checked={values?.role}
                                onChange={handleChange}
                            />
                            <CheckboxLabel
                                name="role"
                                label="Admin"
                                type="checkbox"
                                value="admin"
                                checked={values?.role}
                                onChange={handleChange}
                            />
                            <CheckboxLabel
                                name="role"
                                label="Super Admin"
                                type="checkbox"
                                value="superadmin"
                                checked={values?.role}
                                onChange={handleChange}
                            />
                            <div className="w-full mt-3 mb-6">
                                <p className='text-sm font-semibold text-gray-800 w-1/3 mb-2'>Status</p>
                            </div>
                            <div className="flex items-start mb-6">
                                <div className="flex items-center h-5">
                                    <input name="status" type="checkbox" value={values?.status} 
                                        onChange={handleChange} 
                                        className="w-4.5 h-4.5 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300"
                                        checked={values?.status}
                                    />
                                </div>
                                <label htmlFor="remember" className="ml-2 text-sm font-medium text-gray-900">Status</label>
                            </div>
                            {/* label, type, bgColor, bgHover, color, colorHover, onClick */}
                            <div className="flex gap-2">
                                <Button
                                    label="Reset"
                                    type="reset"
                                    bgColor="#D9435E"
                                    bgHover="#D9435E"
                                    color="white"
                                />
                                <Button
                                    label="Submit"
                                    type="submit"
                                    bgColor="#1ABC9C"
                                    bgHover="#1ABC9C"
                                    color="white"
                                    disabled={isSubmitting}
                                />
                            </div>
                        </form>
                    </div>
                    </>
                }
            </div>
        </Layout>
    )
}

export default ProtectedRoute(add)
