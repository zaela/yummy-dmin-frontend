import { useFormik } from "formik";
import Cookies from "js-cookie";
import Image from "next/image";
import { useRouter } from "next/router";
import { useState } from "react";
import { RiImageAddLine } from "react-icons/ri";
import { useQuery } from "react-query";
import { useSelector } from "react-redux";
import Swal from "sweetalert2";
import { BreadCrumb, Button, ChooseFoto, TextLabel, ProtectedRoute } from "../../../components";
import Layout from "../../../layout";
import { detailUser } from "../../../services/authService";
import { BASE_URL } from "../../../utils/constants";
import { restaurantAddValidation } from "../../../utils/validations/restaurantAddValidation";
import LoadingProduct from "../../products/LoadingProduct";

function detail() {
    const router = useRouter()
    const {id} = router.query;
    const token = Cookies.get('token');
    const [User, setUser] = useState({
        user_image:false,
        name:'',
        location:'',
        status:false,
    })

    const config = {
        headers: { 'Authorization': token },
        params:{id:id}
    }
    
    const { isLoading, isError, error } = useQuery(
        ['users'],
        () => detailUser(config),
        {
            onSuccess: (users) => {
                setUser(users)
            },
            onError: (error) => {
                console.log(error)
            },
        }
    )

    console.log('setUser :', User?.data)

    return (
        <Layout>
            {
                isLoading ?
                <LoadingProduct/>
                :
                <div className='md:mx-11'>
                    <BreadCrumb 
                        directions={[
                        { title:'Detail User', link:'#'},
                        ]}
                    />
                    <div>
                        {/* label, name, placeholder, type, value, required */}
                        <div className="w-full mt-3 mb-6">
                            <div className="w-full max-h-24 justify-center items-center">
                                {
                                    User?.data?.profile_picture ?
                                    <Image width={60} height={60} alt="product image" src={`${BASE_URL}${User?.data?.profile_picture}`}/>
                                    :
                                    <RiImageAddLine className="w-10 h-10 text-gray-400"/>
                                }
                                {User?.data?.profile_picture}
                            </div>
                        </div>
                        <TextLabel
                            label="User Name"
                            value={User?.data?.username}
                        />
                        <TextLabel
                            label="Email"
                            value={User?.data?.email}
                        />
                        <TextLabel
                            label="Role"
                            value={User?.data?.role}
                        />
                        <div className="flex items-start mb-6">
                            <div className="flex items-center h-5">
                                <input name="status" type="checkbox" value={User?.data?.status} 
                                    className="w-4.5 h-4.5 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300"
                                    checked={User?.data?.status}
                                />
                            </div>
                            <label htmlFor="remember" className="ml-2 text-sm font-medium text-gray-900">Status</label>
                        </div>
                        {/* label, type, bgColor, bgHover, color, colorHover, onClick */}
                    </div>
                </div>
            }
        </Layout>
    )
}

export default ProtectedRoute(detail)
