import { Provider } from 'react-redux'
import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist/";
import { ContextProvider } from '../context/ContextProvider'
import '../styles/globals.css'
import Store, { wrapper } from "../redux/store";
import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient();

const persistor = persistStore(Store)

function MyApp({ Component, pageProps }) {
  return (
    <QueryClientProvider client={queryClient}>
      <Provider store={Store}>
        <PersistGate loading={null} persistor={persistor}>
          <ContextProvider>
                <Component {...pageProps} />
          </ContextProvider>
        </PersistGate>
      </Provider>
    </QueryClientProvider>
  )
}

export default MyApp
// export default wrapper.withRedux(MyApp);
