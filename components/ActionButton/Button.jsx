import React from 'react'

function Button({label, type, bgColor, bgHover, color, colorHover, onClick, icon, disabled, width = "w-fit"}) {
  return (
    <button 
      type={type}
      disabled={disabled}
      onClick={onClick}
      className={`${width} px-4 py-2 text-${color} transition-colors duration-150 bg-[${bgColor}] rounded-lg focus:shadow-outline hover:bg-[${bgColor}]/60 ${icon && 'flex gap-1'}`}
    >
      <p>{label}</p> {icon}
    </button>
  )
}

export default Button