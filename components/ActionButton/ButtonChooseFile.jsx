/* eslint-disable no-unused-vars */
import React, { useRef } from 'react';
import { BiPlusCircle } from 'react-icons/bi';

function ButtonChooseFile(props) {
    const fileInput = useRef();
    const selectFile = () => {
        fileInput.current.click();
    }

    return(
        <>
            <input type={'file'} accept={'.png, .jpg, .jpeg'} 
                onChange={(e) => props.uploadfoto(props.index, e.target.files[0], e.target.files[0].size, URL.createObjectURL(e.target.files[0]) )} 
                style={{ "display": "none" }} ref={fileInput}
            />    
            <button type='button' onClick={selectFile} className='ml-2'>
                <BiPlusCircle className='text-[#D9435E] h-7 w-7'/>
            </button>
        </>
    );
};

export default ButtonChooseFile;