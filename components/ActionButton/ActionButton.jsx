import React from 'react'

function ActionButton(props) {
  return (
    <div className='flex gap-4 justify-center'>
        <button className='bg-[#1ABC9C] text-white px-4 py-1.5 rounded-md' onClick={() => props.onClick('detail', props.id)}>
            Detail
        </button>
        <button className='bg-[#FFC700] text-white px-4 py-1.5 rounded-md' onClick={() => props.onClick('edit', props.id)}>
            Edit
        </button>
        <button className='bg-[#D9435E] text-white px-4 py-1.5 rounded-md' onClick={() => props.onClick('delete', props.id)}>
            Delete
        </button>
    </div>
  )
}

export default ActionButton