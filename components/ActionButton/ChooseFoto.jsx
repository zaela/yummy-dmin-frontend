import Image from 'next/image';
import React, {useState, useRef} from 'react';
import { RiImageAddLine } from 'react-icons/ri';
import Swal from 'sweetalert2';
import { BASE_URL } from '../../utils/constants';

const ChooseFoto = (props) => {
  console.log('BASE_URL :', BASE_URL)
    // Custom select file
    const fileInput = useRef();
    const selectFile = () => {
        fileInput.current.click();
    }

    
    const uploadFoto = (e) => {
      const file = e.target.files[0]
      const size = e.target.files[0].size
      if (file) {
        if (size > 2200000) {
          Swal.fire({
            icon: 'error',
            title: 'Upload foto Gagal',
            text: 'Maksimal ukuran file 2 Mb !'
          })
  
        } else {
          // props.uploadfoto(index, file, image_url)
          props.uploadfoto(e)
        }
  
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Upload foto Gagal',
          text: 'Silahkan upload kembali foto!'
        })
      }
    }

    return (
        <>
            <section 
                key={props.index}
                className={
                    props.image.image_url === '' ?
                    `flex flex-col justify-center items-center w-[6rem] h-[6rem] bg-gray-50 rounded-lg border-2 border-dashed border-gray-300 cursor-pointer`
                    :
                    `flex flex-col justify-center items-center w-[6rem] h-[6rem] bg-gray-50 rounded-lg  cursor-pointer`
                }>
                <div className="flex flex-col justify-center items-center p-1 cursor-pointer" onClick={selectFile}>
                {
                    props.image.image_url === '' ?
                    <>
                        <RiImageAddLine className="w-5 h-5 text-gray-400"/>
                        <p className="text-2xs text-gray-500 text-center" style={{fontSize: '0.5rem'}}><span className="font-semibold">Upload File</span></p>
                        <p className="text-2xs text-gray-500 text-center" style={{fontSize: '0.5rem'}}><span className="font-semibold">5mb .jpg .png</span></p>
                        <p className="text-2xs text-gray-500 text-center" style={{fontSize: '0.5rem'}}><span className="font-semibold">.jpeg</span></p>
                    </>:
                    <div className="flex flex-col justify-center items-center pt-5 pb-5">
                        <Image width={80} height={80} alt="variant image" className='w-auto' src={props.image.file === '' ? `${BASE_URL}${props.image.image_url}` : `${props.image.image_url}`}/>
                    </div>
                }
                </div>
                <input type={'file'} accept={'.png, .jpg, .jpeg'} onChange={(e) => {
                  uploadFoto(e, props.image)
                }} style={{ "display": "none" }} ref={fileInput}/>    
            </section>
        </>
    )
}

export default ChooseFoto