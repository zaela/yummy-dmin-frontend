import Link from 'next/link';
import React from 'react';
import { MdKeyboardArrowRight } from 'react-icons/md';
const BreadCrumb = ({ directions, marginBottom }) => (
  <div className={marginBottom ? `mb-${marginBottom}` : "mb-6"}>
    <nav className="flex" aria-label="Breadcrumb">
      <ol className="inline-flex items-center space-x-1 md:space-x-1 text-2xl">
        {
          directions.map((item, i) => {
            return(
              <li className="inline-flex items-center" key={i}>
                {i > 0? <MdKeyboardArrowRight className='text-black font-medium'/> : ''}
                {
                  directions.length === (i+1) ? 
                    i === 0 ?
                    <span className="text-black ml-1 md:ml-1 font-semibold">{item.title}</span>
                    :
                    <span className="text-gray-600 ml-1 md:ml-1 font-semibold">{item.title}</span>
                  :
                    i === 0 ?
                    <Link href={item.link} className="text-black font-semibold hover:text-gray-900 inline-flex items-center">
                      {item.title}
                    </Link>
                    :
                    <Link href={item.link} className="text-gray-700 font-semibold hover:text-gray-900 inline-flex items-center">
                      {item.title}
                    </Link>
                }
              </li>
            )
          })
        }
      </ol>
    </nav>
  </div>
);

export default BreadCrumb;