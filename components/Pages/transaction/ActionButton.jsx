import React from 'react'

function ActionButtonTr(props) {
  const data = props.data
  return (
    <div className='flex gap-4 justify-center'>
      {
        data?.status === 'COMPLETED' ?
        <button className='bg-[#D9435E] text-white px-4 py-1.5 rounded-md' onClick={() => props.onClick('delete', data.id)}>
              Delete
        </button>
        :data?.status === 'PROCESS' ?
        <>
          <button className='bg-[#FFC700] text-white px-4 py-1.5 rounded-md' onClick={() => props.onClick('cancel', data.id)}>
              Cancel
          </button>
          <button className='bg-[#1ABC9C] text-white px-4 py-1.5 rounded-md' onClick={() => props.onClick('complete', data.id)}>
              Complete
          </button>
          <button className='bg-[#D9435E] text-white px-4 py-1.5 rounded-md' onClick={() => props.onClick('delete', data.id)}>
              Delete
          </button>
        </>
        :data?.status === 'CANCELLED' ?
        <button className='bg-[#D9435E] text-white px-4 py-1.5 rounded-md' onClick={() => props.onClick('delete', data.id)}>
              Delete
        </button>
        :<></>
      }
    </div>
  )
}

export default ActionButtonTr