import Image from 'next/image'
import React from 'react'
import { MdAdd } from 'react-icons/md'
import { BASE_URL } from '../../../utils/constants'
import Button from '../../ActionButton/Button'
import ChooseVariantFoto from '../../ActionButton/ChooseVariantFoto'
import Input from '../../Input/Input'

function ProductVariantsDetail({variants}) {
  return (
    <div className='w-full'>
        <div className="flex flex-col">
            <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="py-4 inline-block min-w-full sm:px-6 lg:px-8">
                <div className="overflow-hidden">
                <table className="min-w-full text-center">
                    <thead className="border-b bg-[#FFC700]">
                    <tr>
                        <th scope="col" className="text-sm text-white text-left px-4 py-3">
                        No
                        </th>
                        <th scope="col" className="text-sm text-white text-left px-4 py-3">
                        Variant Image
                        </th>
                        <th scope="col" className="text-sm text-white text-left px-4 py-3">
                        Variant Name
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        {
                            variants.length > 0 && (
                                <>
                                {
                                    variants.map((variant, index) => {
                                        return (
                                            <tr className="bg-white border-b hover:bg-gray-200" key={index}>
                                                <td className="px-4 py-3 text-sm text-gray-900">{index+1}</td>
                                                <td className="text-sm text-gray-900 px-4 py-3">
                                                    <section 
                                                        className={
                                                            variant.variant_image === '' ?
                                                            `flex flex-col justify-center items-center w-16 h-16 bg-gray-50 rounded-lg border-2 border-dashed border-gray-300 cursor-pointer`
                                                            :
                                                            `flex flex-col justify-center items-center w-16 h-16 bg-gray-50 rounded-lg  cursor-pointer`
                                                        }>
                                                        <div className="flex flex-col justify-center items-center p-1 cursor-pointer">
                                                        {
                                                            variant.variant_image === '' ?
                                                            <>
                                                                <RiImageAddLine className="-mb-1 w-5 h-5 text-gray-400"/>
                                                                <p className="text-2xs text-gray-500 text-center -mb-2" style={{fontSize: '0.5rem'}}><span className="font-semibold">Upload File</span></p>
                                                                <p className="text-2xs text-gray-500 text-center -mb-2" style={{fontSize: '0.5rem'}}><span className="font-semibold">5mb .jpg .png</span></p>
                                                                <p className="text-2xs text-gray-500 text-center" style={{fontSize: '0.5rem'}}><span className="font-semibold">.jpeg</span></p>
                                                            </>:
                                                            <div className="flex flex-col justify-center items-center pt-5 pb-5">
                                                                <Image width={80} height={80} className='w-16 h-16' src={`${BASE_URL}${variant.variant_image}`}/>
                                                            </div>
                                                        }
                                                        </div>
                                                    </section>
                                                </td>
                                                <td className="text-sm text-gray-900 px-4 py-3 text-left">
                                                    {variant.variant_name}
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                </>
                            )
                        }
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
    </div>
  )
}

export default ProductVariantsDetail