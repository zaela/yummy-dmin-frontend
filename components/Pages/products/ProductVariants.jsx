import React from 'react'
import { MdAdd } from 'react-icons/md'
import Button from '../../ActionButton/Button'
import ChooseVariantFoto from '../../ActionButton/ChooseVariantFoto'
import Input from '../../Input/Input'

function ProductVariants({variants, variantImages, handleVariantImages, handleVariants, addVariants, deleteVariants, setVariantImage}) {
    console.log('variantImages :', variantImages)
  return (
    <div className='w-full'>
        <div className='w-full flex justify-end h-8'>
            <Button
                label=""
                type="button"
                bgColor="#1ABC9C"
                bgHover="#1ABC9C"
                color="white"
                icon={<MdAdd className="h-5 w-5 font-bold text-white"/>}
                onClick={() => addVariants()}
            />
        </div>

        <div className="flex flex-col">
            <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="py-4 inline-block min-w-full sm:px-6 lg:px-8">
                <div className="overflow-hidden">
                <table className="min-w-full text-center">
                    <thead className="border-b bg-[#FFC700]">
                    <tr>
                        <th scope="col" className="text-sm text-white text-left px-4 py-3">
                        No
                        </th>
                        <th scope="col" className="text-sm text-white text-left px-4 py-3">
                        Variant Image
                        </th>
                        <th scope="col" className="text-sm text-white text-left px-4 py-3">
                        Variant Name
                        </th>
                        <th scope="col" className="text-sm text-white text-left px-4 py-3">
                        Action
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        {
                            variants.length > 0 && (
                                <>
                                {
                                    variants.map((variant, index) => {
                                        console.log('variantImages[index] :', variantImages[index])
                                        return (
                                            <tr className="bg-white border-b hover:bg-gray-200">
                                                <td className="px-4 py-3 text-sm text-gray-900">{index+1}</td>
                                                <td className="text-sm text-gray-900 px-4 py-3">
                                                    <ChooseVariantFoto index={index} image={variantImages[index]} 
                                                        uploadfoto={(index, e) => { handleVariantImages(index, e) }}
                                                    />
                                                    {/* {
                                                        isError(errors?.variants, i)?.variant_image && isTouched(errors?.variants, i)?.variant_image && 
                                                        <p className='text-xs mt-1 w-full text-red-600'>{ isError(errors?.variants, i)?.variant_image}</p>
                                                    } */}
                                                </td>
                                                <td className="text-sm text-gray-900 px-4 py-3">
                                                    <Input
                                                        name={`variants.${index}.variant_name`}
                                                        placeholder="Variant Name"
                                                        type="text"
                                                        value={variant.variant_name}
                                                        onChange={(e) => handleVariants(index, e.target.value)}
                                                    />
                                                </td>
                                                <td className="text-sm text-gray-900 px-4 py-3">
                                                    <Button
                                                        label="Delete"
                                                        type="button"
                                                        bgColor="#D9435E"
                                                        bgHover="#D9435E"
                                                        color="white"
                                                        onClick={() => deleteVariants(index, variant)}
                                                    />
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                </>
                            )
                        }
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
    </div>
  )
}

export default ProductVariants