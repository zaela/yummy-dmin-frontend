export { default as BreadCrumb } from './BreadCrumb/BreadCrumb';
export { default as ActionButton } from './ActionButton/ActionButton';
export { default as Button } from './ActionButton/Button';
export { default as ButtonChooseFile } from './ActionButton/ButtonChooseFile';
export { default as ChooseVariantFoto } from './ActionButton/ChooseVariantFoto';
export { default as ChooseFoto } from './ActionButton/ChooseFoto';
export { default as Input } from './Input/Input';
export { default as TextArea } from './Input/TextArea';
export { default as InputLabel } from './Input/InputLabel';
export { default as SelectLabel } from './Input/SelectLabel';
export { default as TextAreaLabel } from './Input/TextAreaLabel';
export { default as TextLabel } from './Input/TextLabel';
export { default as CheckboxLabel } from './Input/CheckboxLabel';
export { default as ProtectedRoute } from './HOC/ProtectedRoute';
export { default as LoadingTable } from './Table/LoadingTable';

// == pages ===
export { default as ProductVariants } from './Pages/products/ProductVariants';
export { default as ActionButtonTr } from './Pages/transaction/ActionButton';