import Cookies from 'js-cookie';
import {useRouter} from 'next/router'

export default function ProtectedRoute(ProtectedComponent) {
  return (props) => {
    if (typeof window !== 'undefined') {
      const Router = useRouter()
      const accessToken = Cookies.get('token');
      const role = Cookies.get('role');
      if (!accessToken) {
        Router.replace('/login')
        return null
      }
      if (role === "customer") {
        Router.replace('/login')
        return null
      }
      return <ProtectedComponent {...props} />
    }
    return null
  }
}
