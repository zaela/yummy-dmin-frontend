import React from "react";
import {
  MdOutlineKeyboardArrowLeft,
  MdOutlineKeyboardArrowRight,
} from "react-icons/md";

const PaginationComponentTransaction = ({
  page = 1,
  between = 3,
  total,
  limit,
  changePage = (page) => console.log(page),
  next = true,
  last = false,
  ellipsis = 0,
}) => {
  const total_pages = Math.ceil(total / limit);

  between = between < 1 ? 1 : between;
  page = page < 1 ? 1 : page > total_pages ? total_pages : page;
  ellipsis =
    ellipsis < 1 ? 0 : ellipsis + 2 >= between ? between - 2 : ellipsis;

  let positions = Array.from({ length: total_pages }, (_, i) => i);

  const qtd_pages = between * 2 + 1;
  const range =
    total_pages <= qtd_pages
      ? // Show active without slice
        positions
      : page - 1 <= between
      ? // Show active in left
        positions.slice(0, qtd_pages - (ellipsis > 0 ? ellipsis + 1 : 0))
      : page + between >= total_pages
      ? // Show active in right
        positions.slice(
          total_pages - qtd_pages + (ellipsis > 0 ? ellipsis + 1 : 0),
          total_pages
        )
      : // Show active in middle
        positions.slice(
          page - 1 - (between - (ellipsis > 0 ? ellipsis + 1 : 0)),
          page + (between - (ellipsis > 0 ? ellipsis + 1 : 0))
        );

  return total !== null && total > 0 ? (
    <div className="justify-center w-fit flex gap-2">
      {last && (
        <Page
          icon={<MdOutlineKeyboardArrowLeft />}
          onClick={() => (page > 1 ? changePage(1) : {})}
          disabled={page <= 1}
        />
      )}
      {next && (
        <Page
          icon={<MdOutlineKeyboardArrowLeft />}
          onClick={() => (page > 1 ? changePage(page - 1) : {})}
          disabled={page <= 1}
        />
      )}
      {total_pages > between * 2 + 1 &&
        ellipsis > 0 &&
        positions.slice(0, page - 1 <= between ? 0 : ellipsis).map((value) => {
          return (
            <Page
              label={value + 1}
              key={value}
              onClick={() => (value !== page - 1 ? changePage(value + 1) : {})}
            />
          );
        })}
      {
        // Show ellipsis when "page" is bigger than "between"
        total_pages > between * 2 + 1 && ellipsis > 0 && page - 1 > between && (
          <Page label="..." />
        )
      }
      {range.map((value) => {
        return (
          <Page
            label={value + 1}
            key={value}
            active={value === page - 1 ? true : false}
            onClick={() => (value !== page - 1 ? changePage(value + 1) : {})}
          />
        );
      })}
      {
        // Show ellipsis when "page" is lower than "between"
        total_pages > between * 2 + 1 &&
          ellipsis > 0 &&
          page < total_pages - between && <Page label="..." />
      }
      {total_pages > between * 2 + 1 &&
        ellipsis > 0 &&
        positions
          .slice(
            page >= total_pages - between
              ? total_pages
              : total_pages - ellipsis,
            total_pages
          )
          .map((value) => {
            return (
              <Page
                label={value + 1}
                onClick={() =>
                  value !== page - 1 ? changePage(value + 1) : {}
                }
              />
            );
          })}
      {next && (
        <Page
          icon={<MdOutlineKeyboardArrowRight />}
          onClick={() => (page < total_pages ? changePage(page + 1) : {})}
          disabled={page >= total_pages}
        />
      )}
      {last && (
        <Page
          icon={<MdOutlineKeyboardArrowRight />}
          onClick={() => (page < total_pages ? changePage(total_pages) : {})}
          disabled={page >= total_pages}
        />
      )}
    </div>
  ) : (
    <></>
  );
};

const Page = ({ label, active, disabled, icon, onClick }) => {
  return (
    <div
      className={`cursor-pointer w-8 h-8 round-full flex justify-center items-center ${
        active ? "bg-[#FFC700]" : "bg-white"
      } border rounded-full`}
      onClick={() => onClick()}
      disabled={disabled}
    >
      {icon ? (
        icon
      ) : (
        <span
          className={`text-sm mb-0 ${
            active ? "text-white" : "text-[#333333]"
          }`}
        >
          {label}
        </span>
      )}
    </div>
  );
};

export default PaginationComponentTransaction;
