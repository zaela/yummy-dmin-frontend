import React from 'react'

function CheckboxLabel({label, name, placeholder, type, value, required, onChange, touched, error}) {
  return (
    <div className="mb-6 flex items-center gap-2">
        <input type={type} id={label} 
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500" 
            placeholder={placeholder} 
            name={name}
            required={required}
            value={value}
            onChange={(e) => onChange(e)}
        />
        <label htmlFor={label} className="block text-sm font-medium text-gray-900">{label}</label>
        {
          touched && error && <p className='text-sm text-red-500'>{error}</p>
        }
    </div>
  )
}

export default CheckboxLabel