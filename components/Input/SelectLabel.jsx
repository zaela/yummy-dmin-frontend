import React from 'react'

function SelectLabel({label, name, placeholder, type, value, required, onChange, touched, error, data}) {
  return (
    <div className="mb-6">
        <label htmlFor={label} className="block mb-2 text-sm font-medium text-gray-900">{label}</label>
        <select id={name} class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
          name={name}
          required={required}
          value={value}
          onChange={(e) => onChange(e)}
        >
          <option selected>--</option>
          {
            data.map((item) => {
              return  <option value={item.value}>{item.label}</option>
            })
          }
        </select>
        {
          touched && error && <p className='text-sm text-red-500'>{error}</p>
        }
    </div>
  )
}

export default SelectLabel