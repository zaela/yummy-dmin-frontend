import React from 'react'

function Input({label, value}) {
  return (
    <div className="mb-6 flex w-full">
        <label htmlFor={label} className="text-sm font-medium text-gray-900 w-1/3">{label}</label>
        <p className="text-sm font-medium text-gray-900 w-2/3">:  {value}</p>
    </div>
  )
}

export default Input