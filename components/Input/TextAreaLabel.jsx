import React from 'react'

function TextArea({label, name, placeholder, type, value, required, onChange}) {
  return (
    <div className="mb-6">
        <label htmlFor={label} className="block mb-2 text-sm font-medium text-gray-900">{label}</label>
        <textarea type={type} id={label} 
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" 
          placeholder={placeholder} 
          required={required}
          name={name}
          value={value}
          onChange={(e) => onChange(e)}
        />
    </div>
  )
}

export default TextArea