import React, { useState } from 'react'
import { BsEye, BsEyeSlash } from 'react-icons/bs'

function Input({label, name, placeholder, type, value, required, onChange, touched, error}) {
  const [inputType, setInputType] = useState('password')
  return (
    <div className="mb-6">
        <label htmlFor={label} className="block mb-2 text-sm font-medium text-gray-900">{label}</label>
        {
          type === "password" ? (
            <div className="relative">
              <div className='absolute right-2 top-3.5'>
                {inputType === "password" ?
                  <BsEyeSlash className='w-3.5 h-3.5 text-gray-500 cursor-pointer' onClick={() => setInputType("text")}/>
                  :
                  <BsEye className='w-3.5 h-3.5 text-gray-500 cursor-pointer' onClick={() => setInputType("password")}/>
                }
              </div>
              <input type={inputType} id={label} 
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" 
                  placeholder={placeholder} 
                  name={name}
                  required={required}
                  value={value}
                  onChange={(e) => onChange(e)}
              />
            </div>
          ):(
            <input type={type} id={label} 
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" 
                placeholder={placeholder} 
                name={name}
                required={required}
                value={value}
                onChange={(e) => onChange(e)}
            />
          )
        }
       
        {
          touched && error && <p className='text-sm text-red-500'>{error}</p>
        }
    </div>
  )
}

export default Input