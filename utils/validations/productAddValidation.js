import * as yup from 'yup'

export const productAddValidation = yup.object().shape({
    product_images:yup.array().min(1, 'Product image field must have at least 1 items').required('Product image is required'),
    name:yup.string().required('Product name in required').max(100, 'Max 100 characters'),
    price:yup.number().required('Price is required'),
    description:yup.string().required('Description is required').max(255, 'Max 255 characters'),
    variant_images:yup.array().min(1, 'Variant image field must have at least 1 items').required('Variant image is required'),
    variants:yup.array().min(1, 'Variant field must have at least 1 items').required('Variant is required'),
})

