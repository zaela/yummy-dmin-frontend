import * as yup from 'yup'

export const restaurantAddValidation = yup.object().shape({
    // restaurant_images:yup.string().required('Restaurant image is required'),
    name:yup.string().required('Restaurant name in required').max(100, 'Max 100 characters'),
    location:yup.string().required('Restaurant name in required').max(100, 'Max 100 characters'),
})

