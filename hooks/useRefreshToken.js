import { axios } from "../pages/api/axios";
// import useAuth from "../context/useAuth";

const useRefreshToken = () => {
    // const { setAuth } = useAuth();
    const {accessToken, refreshToken} = Cookies.get('token');

    const refresh = async () => {
        const response = await axios.get('/refresh', {
            withCredentials: true
        });
        // setAuth(prev => {
        //     console.log(JSON.stringify(prev));
        //     console.log(response.data.accessToken);
        //     return { ...prev, accessToken: response.data.accessToken }
        // });
        Cookies.set('token', accessToken, { expires: 1 });//expired 1 day
        Cookies.set('refresh_token', refreshToken, { expires: 1 });//expired 1 day
        return response.data.accessToken;
    }
    return refresh;
};

export default useRefreshToken;
