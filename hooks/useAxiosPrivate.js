import axios from "axios";
import Cookies from "js-cookie";
// import { useRouter } from "next/router";
import { refreshToken } from "../services/authService";

// const router = useRouter()
const token = Cookies.get('token')
const axiosApiInstance = axios.create({
  headers: { 
    'Accept': 'application/json',
    'Authorization':token,
  }
});

// Request interceptor for API calls
axiosApiInstance.interceptors.request.use(
  async config => {
    const token = Cookies.get('token');
    config.headers['Authorization'] = `${token}`;
    return config;
  },
  error => {
    Promise.reject(error)
});

// Response interceptor for API calls
axiosApiInstance.interceptors.response.use((response) => {
  return response
}, async function (error) {
    const refresh_token = Cookies.get('refresh_token');
    let originalRequest = error.config;
    if (error.response.status === 403 ) {
      originalRequest._retry = true;
      const data = await refreshToken({data:{token:refresh_token}}); 
      
      Cookies.set('token', data?.accessToken, { expires: 1 });//expired 1 day
      originalRequest.headers['Authorization'] = data?.accessToken;
      return axiosApiInstance(originalRequest);
    }
    else if(error.response.status === 401){
      // router.push('/login')
      window.location.href = "/login";
    }
    return Promise.reject(error);
});

export default axiosApiInstance;