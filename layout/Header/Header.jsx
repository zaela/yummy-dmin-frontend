import Cookies from 'js-cookie';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { AiOutlineMenu } from 'react-icons/ai';
import { RiLogoutCircleRLine } from 'react-icons/ri';
import { useStateContext } from '../../context/ContextProvider';

export default function Header() {
  const router = useRouter()
  // const history = ()
  const { activeMenu, setActiveMenu, screenSize, setScreenSize } = useStateContext()

  const handleCloseSidebar = () => {
    setActiveMenu(!activeMenu)
    // if (activeMenu && screenSize <= 900) {
    // }
  }

  const handleLogout = () => {
    Cookies.remove('id');
    Cookies.remove('token');
    Cookies.remove('role');
    Cookies.remove('refresh_token');
    router.push('/login')
  }

  useEffect(() => {
    const handleResize = () => setScreenSize(window.innerWidth);

    window.addEventListener('resize', handleResize);

    handleResize();

    return () => window.removeEventListener('resize', handleResize);
  }, []);

  useEffect(() => {
    if (screenSize <= 900) {
      setActiveMenu(false);
    } else {
      setActiveMenu(true);
    }
  }, [screenSize]);

  return (
    <nav className={`w-full flex justify-between items-center h-14 bg-[#FFC700] z-[1000]`}>
        <div className='px-5 gap-5 right-0 text-left'>
            <AiOutlineMenu className='text-sm text-white md:text-2xl font-semibold tracking-normal' onClick={() => handleCloseSidebar()}/>
        </div>
        <div className='px-12 gap-5 right-0 text-left'>
            <RiLogoutCircleRLine className='text-lg text-white md:text-xl tracking-normal cursor-pointer' onClick={() => handleLogout()}/>
        </div>
    </nav>
  )
}
