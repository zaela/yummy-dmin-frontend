
export default function Footer() {
  return (
    <section>
      <footer className="w-full bg-black px-12 py-4">
          © 2022 Yummy. All rights reserved
      </footer>
    </section>
  )
}
