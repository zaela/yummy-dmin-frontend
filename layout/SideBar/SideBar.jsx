import React from 'react'
import { MdOutlineCancel, MdOutlineProductionQuantityLimits, MdArrowForwardIos } from 'react-icons/md';
import { BiHomeAlt } from 'react-icons/bi';
import { GrTransaction, GrRestaurant } from 'react-icons/gr';
import { FaUsers } from 'react-icons/fa';
import { useStateContext } from '../../context/ContextProvider';
import Link from 'next/link';
import { useSelector } from 'react-redux';
import Cookies from 'js-cookie';

function SideBar() {
    const {loginReducer} = useSelector((state) => state);
    const {name} = loginReducer
    const role = Cookies.get('role');
    console.log('loginreducer sidebar :', loginReducer)
    const { activeMenu, setActiveMenu, handleClick, isClicked, setIsClicked } = useStateContext()

    const activeLink = 'flex items-center text-md gap-5 pl-2 pr-3 pt-2 pb-2 rounded-lg text-white bg-[#FFC700] hover:text-gray-400  m-2';
    const normalLink = 'flex items-center text-md gap-5 pl-2 pr-3 pt-2 pb-2 rounded-lg text-black bg-white hover:text-gray-400  m-2';

    return (
        <div className="h-screen md:overflow-hidden overflow-auto md:hover:overflow-auto pb-10">
            {
                activeMenu && (
                <>
                    <div className="flex h-14 bg-[#FFC700] items-center text-white">
                        <div className='px-12 text-left'>
                            <h3 className='text-sm md:text-2xl font-semibold tracking-normal'>Yummy</h3>
                        </div>
                    </div>
                    <div className='mt-8 mb-2 ml-3 mr-3'>
                        <div>
                            <p className='text-black m-3 mb-1 mt-4 p-1 text-14'><span className='font-bold'>Yummy</span> - {name}</p>
                        </div>
                    </div>
                    <div className='ml-3 mr-3'>
                        <div className='mt-0'>
                            <ul>
                                <li>
                                    <Link href="/"
                                        style={{ cursor:'pointer' }} 
                                        className={
                                            isClicked.dashboard ? activeLink : normalLink
                                        }
                                        onClick={() => handleClick('dashboard')}
                                    >
                                        <BiHomeAlt/>
                                        <span className="capitalize">
                                            Dashboard
                                        </span> 
                                        <MdArrowForwardIos className='ml-auto'/>
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/products"
                                        style={{ cursor:'pointer' }} 
                                        className={
                                            isClicked.products ? activeLink : normalLink
                                        }
                                        onClick={() => handleClick('products')}
                                    >
                                        <MdOutlineProductionQuantityLimits/>
                                        <span className="capitalize">
                                            Products
                                        </span> 
                                        <MdArrowForwardIos className='ml-auto'/>
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/transactions" style={{ cursor:'pointer' }} 
                                        className={
                                            isClicked.transactions ? activeLink : normalLink
                                        }
                                        onClick={() => handleClick('transactions')}
                                    >
                                        <GrTransaction/>
                                        <span className="capitalize">
                                            Transactions
                                        </span> 
                                        <MdArrowForwardIos className='ml-auto'/>
                                    </Link>
                                </li>
                                <li>
                                     <Link href="/restaurants" style={{ cursor:'pointer' }} 
                                        className={
                                            isClicked.restaurants ? activeLink : normalLink
                                        }
                                        onClick={() => handleClick('restaurants')}
                                    >
                                        <GrRestaurant/>
                                        <span className="capitalize">
                                            Restaurants
                                        </span> 
                                        <MdArrowForwardIos className='ml-auto'/>
                                     </Link>
                                </li>
                                {
                                    role === 'superadmin' && (
                                        <li>
                                            <Link href="/users" style={{ cursor:'pointer' }} 
                                                className={
                                                    isClicked.users ? activeLink : normalLink
                                                }
                                                onClick={() => handleClick('users')}
                                            >
                                                <FaUsers/>
                                                <span className="capitalize">
                                                    Users
                                                </span> 
                                                <MdArrowForwardIos className='ml-auto'/>
                                            </Link>
                                        </li>
                                    )
                                }
                            </ul>
                        </div>
                    </div>
                </>   
                )
            }
        </div>
    )
}

export default SideBar