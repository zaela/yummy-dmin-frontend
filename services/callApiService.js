import axios from "axios";
import Swal from 'sweetalert2';
import axiosApiInstance from "../hooks/useAxiosPrivate";
// import useAxiosPrivate from "../hooks/useAxiosPrivate";

export default async function callAPI({url, method, data }) {
  // const axiosPrivate = useAxiosPrivate();

  console.log('call api :', {
    url,
    method,
    ...data,
  })

  // const response = await axios({
  const response = await axiosApiInstance({
    url,
    method,
    ...data,
  }).then((result) => {
    return result.data
  }).catch((err) => {
    Swal.fire({
      title: 'Failed',
      text: err.response.data.message,
      icon: 'warning',
      customClass: {
        confirmButton: 'btn-yummy',
      }
    });
    return err.response.data
  });

  return response;
}


