import callAPI from "./callApiService";

const ROOT_API = process.env.REACT_APP_API_URL;

export async function getRestaurant(data) {
  
  const url = `${ROOT_API}api/restaurants/get`;

  return callAPI({
    url,
    method: 'GET',
    data,
  });
}

export async function detailRestaurant(data) {
    const url = `${ROOT_API}api/restaurants/detail`;
  
    return callAPI({
      url,
      method: 'GET',
      data,
    });
  }

export async function addRestaurant(data) {
    const url = `${ROOT_API}api/restaurants/add`;
  
    return callAPI({
      url,
      method: 'POST',
      data,
    });
  }
  
export async function editRestaurant(data) {
    const url = `${ROOT_API}api/restaurants/edit`;
  
    return callAPI({
      url,
      method: 'PATCH',
      data,
    });
  }

export async function deleteRestaurant(data) {
    const url = `${ROOT_API}api/restaurants/delete`;
  
    return callAPI({
      url,
      method: 'DELETE',
      data,
    });
  }
  