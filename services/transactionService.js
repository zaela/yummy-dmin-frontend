import callAPI from "./callApiService";

const ROOT_API = process.env.REACT_APP_API_URL;

export async function getTransaction(data) {
  
  const url = `${ROOT_API}api/transactions/get`;

  return callAPI({
    url,
    method: 'GET',
    data,
  });
}

export async function cancelTransaction(data) {
  
  const url = `${ROOT_API}api/transactions/cancel`;

  return callAPI({
    url,
    method: 'PATCH',
    data,
  });
}

export async function completeTransaction(data) {
  
  const url = `${ROOT_API}api/transactions/complete`;
  console.log('completeTransaction :', data)
  return callAPI({
    url,
    method: 'PATCH',
    data,
  });
}

export async function deleteTransaction(data) {
    const url = `${ROOT_API}api/transactions/delete`;
  
    return callAPI({
      url,
      method: 'DELETE',
      data,
    });
  }
  