import callAPI from "./callApiService";

const ROOT_API = process.env.REACT_APP_API_URL;

export async function getUser(data) {
  
  const url = `${ROOT_API}api/user/get`;

  return callAPI({
    url,
    method: 'GET',
    data,
  });
}

export async function detailUser(data) {
    const url = `${ROOT_API}api/user/detail`;
  
    return callAPI({
      url,
      method: 'GET',
      data,
    });
  }
  
export async function editUser(data) {
    const url = `${ROOT_API}api/user/edit`;
  
    return callAPI({
      url,
      method: 'PATCH',
      data,
    });
  }

export async function deleteUser(data) {
    const url = `${ROOT_API}api/user/delete`;
  
    return callAPI({
      url,
      method: 'DELETE',
      data,
    });
  }
  