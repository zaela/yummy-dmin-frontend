import axios from "axios";
import Swal from 'sweetalert2';

export default async function callAPIGlobal({url, method, data }) {

  const response = await axios({
    url,
    method,
    ...data,
  }).then((result) => {
    return result.data
  }).catch((err) => {
    Swal.fire({
      title: 'Failed',
      text: err.response.data.message,
      icon: 'warning',
      customClass: {
        confirmButton: 'btn-yummy',
      }
    });
    return err.response.data
  });

  return response;
}