import callAPI from "./callApiService";

const ROOT_API = process.env.REACT_APP_API_URL;

export async function getProduct(data) {
  
  const url = `${ROOT_API}api/products/get`;

  return callAPI({
    url,
    method: 'GET',
    data,
  });
}

export async function detailProduct(data) {
    const url = `${ROOT_API}api/products/detail`;
  
    return callAPI({
      url,
      method: 'GET',
      data,
    });
  }

export async function addProduct(data) {
    const url = `${ROOT_API}api/products/add`;
  
    return callAPI({
      url,
      method: 'POST',
      data,
    });
  }
  
export async function editProduct(data) {
    const url = `${ROOT_API}api/products/edit`;
  
    return callAPI({
      url,
      method: 'PATCH',
      data,
    });
  }

export async function deleteProduct(data) {
    const url = `${ROOT_API}api/products/delete`;
  
    return callAPI({
      url,
      method: 'DELETE',
      data,
    });
  }
  