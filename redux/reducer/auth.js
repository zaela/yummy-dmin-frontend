const initStateLogin = {
  name: '',
  email: '',
  token: '',
  refresh_token: '',
  role: '',
};

export const loginReducer = (state = initStateLogin, action) => {
  if (action.type === 'ACT_LOGIN') {
    console.log('loginReducer :', {
      ...state,
      name: action.value.name,
      email: action.value.email,
      token: action.value.accessToken,
      refresh_token: action.value.refreshToken,
      role: action.value.role,
    })
    return {
      ...state,
      name: action.value.name,
      email: action.value.email,
      token: action.value.accessToken,
      refresh_token: action.value.refreshToken,
      role: action.value.role,
    };
  }
  return state;
};
