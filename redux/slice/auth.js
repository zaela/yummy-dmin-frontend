import { createSlice } from '@reduxjs/toolkit'
// Slice
const authSlice = createSlice({
  name: 'auth',
  initialState: {
    name: '',
    email: '',
    token: '',
    refresh_token: '',
    role: '',
  },
  reducers: {
    loginSuccess: (state, action) => {
      state.name = action.payload.name;
      state.email = action.payload.email;
      state.token = action.payload.accessToken;
      state.refresh_token = action.payload.refreshToken;
      state.role = action.payload.role;
    },
    logoutSuccess: (state, action) =>  {
        state.name = '';
        state.email = '';
        state.token = '';
        state.refresh_token = '';
        state.role = '';
    },
  },
});

export default authSlice.reducer
export const { loginSuccess, logoutSuccess } = authSlice.actions;