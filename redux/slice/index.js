import {combineReducers} from 'redux';
import loginReducer from '../slice/auth';

const reducer = combineReducers({
  loginReducer,
});

export default reducer;