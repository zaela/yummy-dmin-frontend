import { createSlice } from '@reduxjs/toolkit'
// Slice
const globalSlice = createSlice({
  name: 'global',
  initialState: {
    isError: false,
    message: 'Error',
    isLoading: false,
  },
  reducers: {
    setLoading: (state, action) => {
      state.isLoading = action.payload.isLoading;
    },
    setError: (state, action) => {
      state.isError = action.payload.isError;
      state.message = action.payload.message;
    },
  },
});

export default globalSlice.reducer
export const { setLoading, setError } = globalSlice.actions;