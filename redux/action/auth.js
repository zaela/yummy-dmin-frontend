export const ACT_LOGIN = "ACT_LOGIN";

export const actLogin = data => dispatch => {
    dispatch({
        type : ACT_LOGIN,
        value : data
    })
}