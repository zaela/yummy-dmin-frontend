import { applyMiddleware, legacy_createStore as createStore } from "redux";
import {
    FLUSH, PAUSE,
    PERSIST, persistReducer, PURGE,
    REGISTER, REHYDRATE
} from 'redux-persist';
import storage from "redux-persist/lib/storage";
// import reducer from './reducer';
import { configureStore } from "@reduxjs/toolkit";
import reducer from './slice';
import thunk from "redux-thunk";


const persistConfig = {
    key: 'root',
    version: 1,
    storage,
  }


const persistedReducer = persistReducer(persistConfig, reducer);
// const store = createStore(persistedReducer, applyMiddleware(thunk));

const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        serializableCheck: {
          ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
        },
      }),
  })

export default store;
