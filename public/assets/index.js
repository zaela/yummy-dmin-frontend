import Logo from './spices-logo.png';
import Banner from './spices-banner.jpg';
import Product from './spices-product.jpg';
import Product1 from './spices-product1.jpg';
import Product2 from './turmeric.jpg';
import Team1 from './team1.jpg';
import Team2 from './team2.jpg';
import Team3 from './team3.jpg';
import Logo1 from './spices-logo.ico';
import Whatsapp from './whatsapp.json';
import Integrity from './integrity.svg';
import Quality from './quality.svg';
import Commitment from './commitment.svg';
import Innovation from './innovation.svg';

export {Logo, Banner, Product, Product1, Product2, Team1, Team2, Team3, Logo1, Whatsapp, Integrity, Quality, Commitment, Innovation} 