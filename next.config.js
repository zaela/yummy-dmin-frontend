/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    REACT_APP_API_URL:'http://127.0.0.1:4000/',
    REACT_APP_BASE_URL:'http://127.0.0.1:4000/',
    REACT_APP_SECRET_KEY:'56f4d830746f0bbb4a64ce6c75b6c423db5ad2a2e8fcec2897f1c83422a08a84d30cb14feeea54b480ff0b04173e0e595a9b62b79639ae5d5148aef2ad2b6c22'
  },
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: 'http',
        hostname: '127.0.0.1',
        port: '4000',
        pathname: '/assets/products/**',
      },
      {
        protocol: 'http',
        hostname: '127.0.0.1',
        port: '4000',
        pathname: '/assets/restaurants/**',
      },
      {
        protocol: 'http',
        hostname: '127.0.0.1',
        port: '4000',
        pathname: '/assets/storages/**',
      },
    ],
  },
}

module.exports = nextConfig
