import React, { createContext, useContext, useState } from 'react';

const StateContext = createContext();

const initialState = {
  products: false,
  transactions: false,
  restaurants: false,
  users: false,
};

export const ContextProvider = ({ children }) => {
  const [screenSize, setScreenSize] = useState(undefined);
  const [activeMenu, setActiveMenu] = useState(true);
  const [isClicked, setIsClicked] = useState(initialState);
  const [auth, setAuth] = useState({});

  const handleClick = (clicked) => setIsClicked({ ...initialState, [clicked]: true });

  return (
    <StateContext.Provider value={{ activeMenu, setActiveMenu, screenSize, setScreenSize, handleClick, isClicked, setIsClicked, auth, setAuth }}>
      {children}
    </StateContext.Provider>
  );
};

export const useStateContext = () => useContext(StateContext);
